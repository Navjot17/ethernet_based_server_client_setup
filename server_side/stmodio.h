#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <modbus/modbus.h>
#include <math.h>
#include "common.h"

#define 	LOOP		1
#define		SLAVE		0x02
#define		SLAVE_SWITCH	0x03
#define		ADDRESS_START	0
#define		ADDRESS_END	30

enum	modbusfunctions {
			READREG_MB, 
			WRITEREG_MB, 
			READCOIL_MB, 
			WRITECOIL_MB, 
			GETSTAT_MB} ;

typedef	struct	modbusdev
	{
	int 	devid;
	boolean	active;
	int	error_status;
	int	busnum;
	int	nslave;
	int 	address;
	int	function;
	int	dat;
	struct	modbusdev *nextmoddev;
	} modbusdev, *modbusdevptr;

void	init_modbus (void);
#ifdef __cplusplus
}
#endif
