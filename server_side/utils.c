/*****UTILS.C*****/
#include <stdio.h>
#include <stdlib.h>
#include "pserv.h"

boolean bitstatus(logptr logp)
{
int32	i;
i = logp->cnap->intdat & bitmask[logp->bitpos];
if(i)
	return TRUE;
return FALSE;
}

void bitset(logptr logp)
{
logp->cnap->intdat |= bitmask[logp->bitpos];
}

void bitclr(logptr logp)
{
logp->cnap->intdat &= ~bitmask[logp->bitpos];
}

void 	error_exit(charptr p)
{
perror(p);
exit(0);
}
