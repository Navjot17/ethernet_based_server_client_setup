/*****SHMEM.C*****/
#include <sys/ioctl.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <string.h>

#include "pserv.h"
#include "stmodio.h"

#define	SHHEAPSIZE	1000000		/* 1 MB shared memory */

serv_infoptr	info;

extern uint8_t *tab_rq_status;
extern uint8_t *tab_rp_status;
extern int16_t *tab_rq_registers;
extern int16_t *tab_rp_registers;
extern int nb = 8;
char	ipc_key[] = "ipckey";
char	ipc_id = 'C';

void kill_shmem()
{
	key_t	key;
	int	id;

	key = ftok (ipc_key, ipc_id);
	id = shmget (key, 0, 0);
	if (shmctl (id, IPC_RMID, NULL) < 0)
		perror("kill_shm");
/*
	else
		fprintf (stderr, "Detatched shared memory\n");
*/
}

char *init_shmem(int size)
{
FILE	*fp;
key_t	mykey;
int	id;
int 	flags = 0777 | IPC_CREAT;
char	*shared_mem;

kill_shmem(); 

fp = fopen(ipc_key,"w"); 
if(!fp) 
	error_exit("fopen");
fclose(fp);	
mykey = ftok(ipc_key, ipc_id);
if(mykey < 0) 
	error_exit("ftok");
id = shmget(mykey,size,flags);
if(id < 0) 
	error_exit("shmget");
shared_mem = shmat(id,0,0);
if(shared_mem == (char *) -1)
	error_exit("shmat");

tab_rq_status = (uint8_t *) malloc (nb * sizeof (uint8_t));
memset (tab_rq_status, 0, nb * sizeof (uint8_t));

tab_rp_status = (uint8_t *) malloc (nb * sizeof (uint8_t));
memset (tab_rp_status, 0, nb * sizeof (uint8_t));
return shared_mem;
}


static	void	*base, *current, *top;

void init_shared_memory()
{
charptr memstart;
int 	size;

size = SHHEAPSIZE + sizeof(serv_info);

memstart = init_shmem(size);
memset(memstart, 0 , size);		/* must be done */
info = (void *) memstart;

base = memstart + sizeof(serv_info);
current = base;
top = current + SHHEAPSIZE;

/*
printf("Shared Heap from %p to %p. %d bytes.\n",base, top, top-base);
*/
}

void	*shmalloc(size_t size)
{
void	*tmp;

if( (current + size) >= top)
	return NULL;

tmp = current;
current += size;
return tmp;
}


