/*****SWITCH.C*****/
#include <time.h>
#include <stdio.h>
#include <string.h>
#include "pserv.h"
#include <modbus/modbus.h>
#include "stmodio.h"

#define		MAXCAVITY	30

static	char	momp_label[10];
static	logptr	active_momp = NULL;
boolean	pushing = FALSE;
extern time_t	ct;
extern uint8_t *tab_rq_status;
extern uint8_t *tab_rp_status;
extern uint16_t *tab_rq_registers;
extern uint16_t *tab_rp_registers;
extern int ret, nb_fail, nb_loop, addr, nb, selection_QWR;

extern int	data_in [8];
extern float 	data_in_f [8];
extern int	nslave;
extern int 	timer_test_ticks;
extern modbus_param_t mbparam;

extern int i;
extern int ret, nb_fail, nb_loop, addr, nb, selection_QWR;

extern char *Cavity_labels []; 


/* 
	If Client Fails to Put off a Momentory Switch, Monitor does it.
	Only if it sees it ON twice without anybody pushing it.
 */

void momswitch_monitor()
{
if(active_momp && !pushing)
	{
	bitclr(active_momp);
	cp_camio(active_momp->cnap);

	time(&ct);
//	printf("MSWM:%s %s %s going to %s state @ %s", momp_label,functypary[active_momp->func],
//	rangetypary[active_momp->hi], rangetypary[active_momp->lo],ctime(&ct));

	active_momp = NULL;
	momp_label[0] = '\0';
	}
pushing = FALSE;

printf("MSMonitor.. first visit\n");
}

/* Depending on result returns the status code to be send to the client */


int switch_off_momentory(charptr label, logptr logp)
{

printf("In off Momsw\n");

if(!active_momp)
	{
	printf("No momentory pressed yet..\n");
	return DONE;
	}

if(logp != active_momp)
	{
	printf("Busy with some other momentory type signal\n");
	return JUST_FAILED;
	}
	
pushing = FALSE;
bitclr(logp);
cp_camio(logp->cnap);
if(logp->cnap->error_status)
	return CAMIO_ERROR;
time(&ct);
//	printf("M:%s %s %s going to %s state @ %s", label,functypary[logp->func],
//	unitypary[logp->unit], rangetypary[logp->lo],ctime(&ct));
active_momp = NULL;
momp_label[0] = '\0';

return DONE;
}


int switch_on_momentory(charptr label, logptr logp)
{
if(active_momp)			/* something has been pushed earlier*/ 
	{
	if(active_momp == logp)
		{
		printf("Just Extending %s\n",label);
		pushing = TRUE;
		return DONE;
		}
	else
		{
		printf("Busy with some other momentory type signal\n");
		return 	JUST_FAILED; 
		}
	}		
		
bitset(logp);			/* A fresh assignment */
cp_camio(logp->cnap);
if(logp->cnap->error_status)
	return CAMIO_ERROR;
time(&ct);
//	printf("M:%s %s %s going to %s state @ %s", label,functypary[logp->func],
//	unitypary[logp->unit], rangetypary[logp->hi],ctime(&ct));
active_momp = logp;
strcpy(momp_label,label); 
return DONE;
}

int pulse_switch(charptr label, logptr logp)
{
logp->cnap->active = TRUE;
cp_camio(logp->cnap);
logp->cnap->active = FALSE;
if(logp->cnap->error_status)
	return CAMIO_ERROR;
time(&ct);
//	printf("P:%s %s %s going to %s state@ %s", label,functypary[logp->func],
//	unitypary[logp->unit], rangetypary[logp->hi],ctime(&ct));
return DONE;
}

int switch_on_toggle(charptr label, logptr logp)
{
if(nlk_violation(logp))
	return NLK_VIOLATION;
bitset(logp);
cp_camio(logp->cnap);
if(logp->cnap->error_status)
	return CAMIO_ERROR;
time(&ct);
//	printf("T:%s %s %s going to %s state @ %s", label,functypary[logp->func],
//	unitypary[logp->unit], rangetypary[logp->hi],ctime(&ct));
return DONE;
}	

int switch_off_toggle(charptr label, logptr logp)
{
bitclr(logp);
cp_camio(logp->cnap);
if(logp->cnap->error_status)
	return CAMIO_ERROR;
time(&ct);
//	printf("T:%s %s %s going to %s state @ %s", label,functypary[logp->func],
//	unitypary[logp->unit], rangetypary[logp->lo],ctime(&ct));
return DONE;
}

boolean	problem_cases(cmdpackptr cp)
{
logptr logp = locate_logical(cp->label, cp->ascfunc, cp->ascunit);
if(!logp)
	{
	cp->result = LOG_NOT_FOUND;
	return TRUE;
	}

if( !strcmp(cp->label,"PBPTK1") && !strcmp(cp->ascfunc,"SC") &&
	 !strcmp(cp->ascunit,"CTL") &&  !strcmp(cp->ascstate,"CHG") )
	{
	printf("M:%s %s %s: Going %s for 50 ms @ %s", cp->label,
	functypary[logp->func], unitypary[logp->unit], 
	rangetypary[logp->hi], ctime(&ct));
	
	bitset(logp);
	cp_camio(logp->cnap);
	if(logp->cnap->error_status)
		{
		cp->result = CAMIO_ERROR;
		return TRUE;
		}
	usleep(50000);		/* Push it for 50 msecs */
	bitclr(logp);
	cp_camio(logp->cnap);
	if(logp->cnap->error_status)
		{
		cp->result = CAMIO_ERROR;
		return TRUE;
		}
	cp->result = DONE;
	return TRUE;
	}

if( !strcmp(cp->label,"GS T 1") && !strcmp(cp->ascfunc,"SC") &&
	 !strcmp(cp->ascunit,"CTL") &&  !strcmp(cp->ascstate,"CHG") )
	{
	printf("M:%s %s %s: Going %s for 50 ms @ %s", cp->label,
	functypary[logp->func], unitypary[logp->unit], 
	rangetypary[logp->hi], ctime(&ct));

	bitset(logp);
	cp_camio(logp->cnap);
	if(logp->cnap->error_status)
		{
		cp->result = CAMIO_ERROR;
		return TRUE;
		}
	usleep(50000);
	bitclr(logp);
	cp_camio(logp->cnap);
	if(logp->cnap->error_status)
		{
		cp->result = CAMIO_ERROR;
		return TRUE;
		}
	cp->result = DONE;
	return TRUE;
	}
return FALSE;
}


boolean nlk_violation(logptr logp)
{
return FALSE;
}

int mod_switch_on_toggle(charptr label, logptr logp)
{
if(nlk_violation(logp))
	return NLK_VIOLATION;
bitset(logp);
//cp_camio(logp->cnap); 		//ORIGINAL CAMAC ROUTINES
//if(logp->cnap->error_status)		//ORIGINAL CAMAC ROUTINES
//return CAMIO_ERROR;			//ORIGINAL CAMAC ROUTINES
time(&ct);
//	printf("T:%s %s %s going to %s state @ %s", label,functypary[logp->func],
//	unitypary[logp->unit], rangetypary[logp->hi],ctime(&ct));
return DONE;
}	

int setmodsw_on (cmdpackptr cp)
{
int i;
int selection = 2; 
float dummy = 1.011;
nb = 8;
addr = 0;

//tab_rq_status = (uint8_t *) malloc (nb * sizeof (uint8_t));
memset (tab_rq_status, 0, nb * sizeof (uint8_t));

//tab_rp_status = (uint8_t *) malloc (nb * sizeof (uint8_t));
memset (tab_rp_status, 0, nb * sizeof (uint8_t));

//printf ("=========%s.\n", cp-> label);

for ( selection = 0; strcmp (cp->label, Cavity_labels[selection]) && (selection < MAXCAVITY); ++selection)
//	printf ("%d %s %s.\n", selection, cp->label, Cavity_labels[selection]);
	;

//printf ("Selection is %d.\n", selection);
	if (selection>=MAXCAVITY) 
		return 0;
switch (selection+1) {
	case 1:
		nslave =3;
		tab_rq_status [0] = 1;
		break;
	case 2:
		nslave =3;
		tab_rq_status [1] = 1;
		break;
	case 4:
		nslave =3;
		tab_rq_status [2] = 1;
		break;
	case 5:
		nslave =3;
		tab_rq_status [3] = 1;
		break;
	case 7:
		nslave =3;
		tab_rq_status [4] = 1;
		break;
	case 8:
		nslave =3;
		tab_rq_status [5] = 1;
		break;
	case 10:
		nslave =4;
		tab_rq_status [0] = 1;
		break;
	case 11:
		nslave =4;
		tab_rq_status [1] = 1;
		break;
	case 13:
		nslave =4;
		tab_rq_status [2] = 1;
		break;
	case 14:
		nslave =4;
		tab_rq_status [3] = 1;
		break;
	case 16:
		nslave =4;
		tab_rq_status [4] = 1;
		break;
	case 17:
		nslave =4;
		tab_rq_status [5] = 1;
		break;
	case 19:
		nslave =5;
		tab_rq_status [0] = 1;
		break;
	case 20:
		nslave =5;
		tab_rq_status [1] = 1;
		break;
	case 22:
		nslave =5;
		tab_rq_status [2] = 1;
		break;
	case 23:
		nslave =5;
		tab_rq_status [3] = 1;
		break;
	default:
		break;
	}	
	ret = force_multiple_coils (&mbparam, nslave, addr, nb, tab_rq_status);
//	for (i=0; 1<5; ++i)
			dummy *= 1.011;
			dummy *= 1.011;
			dummy *= 1.011;
	if (ret != nb) {
//		printf ("ERRROR read_coil_status\n");
//		printf ("Slave = %d, address = %d, nb = %d\n", nslave, addr, nb, tab_rp_status);
//		if (ret !=nb){
//			for (i=0; i<nb; i++) {
//			if (tab_rp_status [i] != tab_rq_status [i])
//			{
//			printf ("Slave = %d, address = %d, value = %d(0x%X) != %d (0x%X) \n", 
//			nslave, addr,
//			tab_rq_status[i], tab_rq_status[i],
//			tab_rp_status[i], tab_rp_status[i]);
//		nb_fail++;
//				}
//			}
//		}
	}
}

int setmodsw_off (cmdpackptr cp)
{
int i;
int selection = 2; 
float dummy = 1.011;
nb = 8;
addr = 0;

//tab_rq_status = (uint8_t *) malloc (nb * sizeof (uint8_t));
memset (tab_rq_status, 0, nb * sizeof (uint8_t));

//tab_rp_status = (uint8_t *) malloc (nb * sizeof (uint8_t));
memset (tab_rp_status, 0, nb * sizeof (uint8_t));

//printf ("=========%s.\n", cp-> label);

for ( selection = 0; strcmp (cp->label, Cavity_labels[selection]) && (selection  < MAXCAVITY); ++selection)
	;
//	printf ("%d %s %s.\n", selection, cp->label, Cavity_labels[selection]);
//	printf ("Selection is %d.\n", selection);
	if (selection>=MAXCAVITY) 
		return 0;

switch (selection+1) {
	case 1:
		nslave =3;
		tab_rq_status [0] = 0;
		break;
	case 2:
		nslave =3;
		tab_rq_status [1] = 0;
		break;
	case 4:
		nslave =3;
		tab_rq_status [2] = 0;
		break;
	case 5:
		nslave =3;
		tab_rq_status [3] = 0;
		break;
	case 7:
		nslave =3;
		tab_rq_status [4] = 0;
		break;
	case 8:
		nslave =3;
		tab_rq_status [5] = 0;
		break;
	case 10:
		nslave =4;
		tab_rq_status [0] = 0;
		break;
	case 11:
		nslave =4;
		tab_rq_status [1] = 0;
		break;
	case 13:
		nslave =4;
		tab_rq_status [2] = 0;
		break;
	case 14:
		nslave =4;
		tab_rq_status [3] = 0;
		break;
	case 16:
		nslave =4;
		tab_rq_status [4] = 0;
		break;
	case 17:
		nslave =4;
		tab_rq_status [5] = 0;
		break;
	case 19:
		nslave =5;
		tab_rq_status [0] = 0;
		break;
	case 20:
		nslave =5;
		tab_rq_status [1] = 0;
		break;
	case 22:
		nslave =5;
		tab_rq_status [2] = 0;
		break;
	case 23:
		nslave =5;
		tab_rq_status [3] = 0;
		break;
	default:
		break;
	}	
	ret = force_multiple_coils (&mbparam, nslave, addr, nb, tab_rq_status);
//	for (i=0; i<5; ++i)
		dummy *= 1.011;
		dummy *= 1.011;

	if (ret != nb) {
//		printf ("ERROR read_coil_status\n");
//		printf ("Slave = %d, address = %d, nb = %d\n", nslave, addr, nb, tab_rp_status);
/*		if (ret !=nb){
			for (i=0; i<nb; i++) {
			if (tab_rp_status [i] != tab_rq_status [i])
			{
			printf ("Slave = %d, address = %d, value = %d(0x%X) != %d (0x%X) \n", 
			nslave, addr,
			tab_rq_status[i], tab_rq_status[i],
			tab_rp_status[i], tab_rp_status[i]);
		nb_fail++;
				}
			}
		}*/
	}
}
