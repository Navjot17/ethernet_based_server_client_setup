/*****DBCHAIN.C*****/
#include <string.h>
#include <stdio.h>
#include "pserv.h"

extern	headptr	head;
labptr	locate_label(charptr);

headptr create_head()
{
headptr p;

p = (headptr) shmalloc(sizeof(headrec));
if(p)
	{
	strcpy(p->userfileid,DBASEID);
	p->firstcnaf = NULL;
	p->firstlab = NULL;
	return p;
	}
else
	perror("shmalloc");
return NULL;
}

/*____________________________related to cnaf record_______________________*/

cnafptr	locate_cnaf(byte crate, int16 naf)
{
cnafptr p = head->firstcnaf;

while(p != NULL)
	if((p->crate == crate) && (p->naf == naf) )
		return(p);
	else
		p = p->nxtcnaf;
return(NULL);
}

cnafptr create_cnaf(byte crate, int16 naf)
{
cnafptr p;

p = (cnafptr) shmalloc(sizeof(cnafrec));
if(p != NULL)
	{
	p->active = TRUE;
	p->crate = crate;
	p->naf = naf;
	p->intdat = 0L;
	p->nxtcnaf = NULL;
	return p;
	}
else
	perror("shmalloc");
return NULL;
}

void	attach_cnaf(cnafptr newcp)
{

if(locate_cnaf(newcp->crate, newcp->naf))
	error_exit("Duplicate CNAF");



if(head->firstcnaf == NULL)
	{
	head->firstcnaf = newcp;
	return;
	}
else if( (newcp->naf) < head->firstcnaf->naf)
	{
	newcp->nxtcnaf = head->firstcnaf;
	head->firstcnaf = newcp;
	return;
	}
else
	{
	cnafptr p;
	p = head->firstcnaf;
	while(p->nxtcnaf != NULL)
		if(newcp->naf < p->nxtcnaf->naf)
			{
			newcp->nxtcnaf = p->nxtcnaf;
			p->nxtcnaf = newcp;
			return;
			}
		else
			p = p->nxtcnaf;
	p->nxtcnaf = newcp;
	}
}

/*____________________related to logical descriptors_______________________*/

logptr	locate_logical(charptr lab, charptr ascfunc, charptr ascunit)
{
labptr labp;
logptr logp;

labp = locate_label(lab);
if(!labp)
	return NULL;

logp = labp->firstlog;
while(logp != NULL)
	if( !strcmp(ascfunc, functypary[logp->func]) && 
	    !strcmp(ascunit, unitypary[logp->unit]) )
		return logp;
	else
		logp = logp->nxtlog;
return NULL;
}

void	attach_logical(labptr labp,logptr newlogp)
{
logptr p;
p = labp->firstlog;


if(p == NULL){
	labp->firstlog = newlogp;
}
else
	{
	while(p->nxtlog){
		p = p->nxtlog;
	}
	p->nxtlog = newlogp;
	}
}

logptr	create_logical()
{
logptr p;

p = (logptr) shmalloc(sizeof(logrec));
if(p)
	{
	p->nxtlog = NULL;
	p->cnap = NULL;
	return p;
	}
else
	perror("shmalloc");
return NULL;
}


/*__________________related to numerical descriptors_______________________*/

numptr	locate_numerical(charptr lab, charptr ascfunc, charptr ascunit)
{
numptr 	nump;
labptr	labp;

labp = locate_label(lab);
if(!labp)
	return NULL;

nump = labp->firstnum;
while(nump)
	if( !strcmp(ascfunc, functypary[nump->func]) && 
	    !strcmp(ascunit, unitypary[nump->unit]) )
		return nump;
	else
		nump = nump->nxtnum;
return NULL;
}

void	attach_numerical(labptr labp,numptr newnump)
{
numptr p;

if(labp->firstnum == NULL)
	labp->firstnum = newnump;
else
	{
	p = labp->firstnum;
	while(p->nxtnum) p = p->nxtnum;
	p->nxtnum = newnump;
	}
}

numptr	create_numerical()
{
numptr p;

p = (numptr) shmalloc(sizeof(numrec));
if(p)
	{
	p->nxtnum = NULL;
	p->cnap = NULL;
	return p;
	}
else
	perror("shmalloc");
return NULL;
}

/*________________________related to label record__________________________*/

labptr	locate_label(charptr curlab)	/* look for a given label */
{
labptr p = head->firstlab;
while(p)
	if(!strcmp(p->labloc,curlab) )
		return(p);
	else
		p = p->nxtlab;
return(NULL);
}

labptr create_label(charptr newlabloc)	/*allocate memory for label record */
{
labptr p;

p = (labptr) shmalloc(sizeof(labrec));
if(p)
	{
	strcpy(p->labloc,newlabloc);
	p->firstnum = NULL;
	p->firstlog = NULL;
	p->nxtlab = NULL;
	return p;
	}
else
	perror("shmalloc");
return NULL;
}

void	attach_label(labptr newlabp)	/* attach to the label chain */
{
if(head->firstlab == NULL)
	{
//	printf ("I am attaching when head point firstlab is NULL %d.\n", newlabp->nxtlab);
	head->firstlab = newlabp;
	return;
	}
else if( strcmp(newlabp->labloc,head->firstlab->labloc) < 0 )
	{
//	printf ("I am attaching when head point firstlab is not NULL.\n");
	newlabp->nxtlab = head->firstlab;
	head->firstlab = newlabp;
	return;
	}
else
	{
	labptr p;
	p = head->firstlab;
	while(p->nxtlab)
		if( strcmp(newlabp->labloc,p->nxtlab->labloc) < 0 )
			{
			newlabp->nxtlab = p->nxtlab;
			p->nxtlab = newlabp;
			return;
			}
		else
			p = p->nxtlab;
	p->nxtlab = newlabp;
	}
}

labptr create_attach_label(charptr newlabloc)
{
labptr labp = create_label(newlabloc);
if(labp)
	attach_label(labp);
return labp;
}
