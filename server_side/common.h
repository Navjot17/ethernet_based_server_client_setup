#ifdef __cplusplus
extern "C" {
#endif

#ifndef	__COMMON__
#define	__COMMON__
#define CMD_PORT	2000
#define DAT_PORT	2002

#define	USERNAME_SIZE	10
#define	PASSWD_SIZE	10
#define LABLEN  	10
#define FUNCLEN  	5
#define UNITLEN  	5
#define STATELEN  	5
#define FORMLEN  	5
#define BPM_NAMELEN	10
#define BPM_POINTS	1024

typedef	unsigned char	byte;
typedef byte	 boolean;
typedef	unsigned long int32;
typedef	unsigned short int16;
typedef	double	real;
typedef	char	*charptr;
typedef	byte	*byteptr;
typedef int16	*i16ptr;
typedef	int	*intptr;
typedef int32	*i32ptr;
typedef	real	*realptr;

enum commands_vals { PASSWD, 
		CLOSE, 
		GET_STATE, 
		GET_STATE_NOW,
		SET_STATE, 
		GET_VAL,
		GET_VAL_NOW, 
		SET_VAL, 
		GET_SWTYPE, 
		GET_LOW, 
		GET_HIGH, 
		GET_MIN, 
		GET_MAX,
		GET_FORM,
		LOCATE_NUMSIG,
		LOCATE_LOGSIG,
		LOCATE_BPM,
		BPM_IN,
		BPM_OUT,
		BPM_READ,
		TEST_SOCKET,
		lastcmd };

enum results_vals { DONE, BAD_COMMAND, UNKNOWN_HOST , WRONG_PASSWD, 
	TOO_MANY_CL, LAB_NOT_FOUND, NUM_NOT_FOUND, LOG_NOT_FOUND, 
	CAMIO_ERROR, ULIM_ERROR, LLIM_ERROR, DSIZE_ERROR, SWTYPE_ERROR, 
	INVALID_STATE, NLK_VIOLATION, LOG_READONLY, NUM_READONLY,
	UNKNOWN_DTYPE, IPH_ERROR, IGC_ERROR, FSDHI_ERROR, 
	BPM_NOT_FOUND,
	BPM_NOT_ACTIVE, 
	BPM_NOT_IN, 
	BPM_READ_ERR,
	JUST_FAILED, 
	lastres};


typedef	struct			/* The command and reply packet */
	{
	int	command;	//	enum	commands command;
	int	result;		//enum	results	result;
	short	count;
	char	label[LABLEN];
	char	ascfunc[FUNCLEN];
	char	ascunit[UNITLEN];
	char	ascstate[STATELEN];
	double	value;
	}cmdpack, *cmdpackptr;



/* eums used at the client as well */

enum	functype_vals {firstfunc, StatusControl, StatusReadr, VoltageControl,
	VoltageRead, VoltageReadNeg, CurrentControl, CurrentRead,
	DisplacementControl, DisplacementRead, 
	MagnetOn, MagnetOff, PressureRead,PressureReadLow,
	PressureReadHigh,
	TempRead, FrequencyRead, WidthControl, lastfunc};

enum	unitype_vals {firstunit,Ampere,Atmos,Alarm,Bypass,Camac,
	Cable, CamacRR, Close,Centigrade, Centimeter,Control,
	Decrement, Direction, 
	FoilStripperHigh, FoilStripperLow, Filament, 
	Ground, Inside, Increment, KiloGram, KiloVolt,Local, 
	MilliAmp, Millimeter, MilliVolt, 
	BuncherMode, Normal, NanoSecond, SwitchOff, SwitchOn,
	OutSide, Overload, OvertmpPS, OvertmpMagnet,
	Percent, PSA, PSIG, Pos, PowerFail, Power,
	Torr, Turn, Micron, MicroAmp, Open, Volt, Interlock, MicroSecond,
	OverMV, UnderMV, XKiloVolt, YKiloVolt, AmpLeft, AmpRight, AmpUp, 
	AmpDown, Hertz, MinLimit, MaxLimit,TransistorFail, Ready,
	WaterFailMagnet, WaterFailPS, 
	SlitMode, GvmMode, RotShaft1, RotShaft2, RawValue, lastunit};

enum    swtype_vals {firstsw, togsw, momsw, pulsw, lastsw};

enum	formtype_vals {firstform,f0,f1,f2,f3,e0,e1,e2,ezero,i0,pu,bun,lastform};

		/* global variables useful at both ends. */
extern	const	charptr command_strings[];
extern	const	charptr result_strings[];

#endif
#ifdef __cplusplus
}
#endif
