/*****NEWCON.C*****/
#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <stdio.h>
#include <string.h>

#include "pserv.h"

typedef	struct iprec {
	char	hostaddr[16];
	char	user[USERNAME_SIZE];
	char	passwd[PASSWD_SIZE];
	struct	iprec *next;
	}iprec, *iprecptr;
	
char		ipfile[]="/usr/home/wlserv/stserv/iplist.dat";
iprecptr	iphead;

boolean	load_iplist()
{
char		hostaddr[16];
char		user[USERNAME_SIZE];
char		passwd[PASSWD_SIZE];
FILE		*fp;
iprecptr	ip;

fp = fopen(ipfile,"rt");
if(!fp)
	return FALSE;
while(!feof(fp))
	{
	fscanf(fp,"%s%s%s",hostaddr,user,passwd);
	if(hostaddr[0] == '$')
		break;
	ip = (iprecptr) malloc(sizeof(iprec));
	if(!ip)
		error_exit("No memory");

	strcpy(ip->hostaddr,hostaddr);	
	strcpy(ip->user,user);	
	strcpy(ip->passwd,passwd);	
	ip->next = NULL;
	
	ip->next = iphead;
	iphead = ip;
	}
fclose(fp);
return TRUE;
}

boolean knownhost(charptr p)
{
iprecptr ip = iphead;
while(ip)
	if(!strcmp(ip->hostaddr,p))
		return TRUE;
	else
		ip = ip->next;
return FALSE;
}

boolean authorized_user(charptr host, charptr user, charptr pw)
{
iprecptr ip = iphead;

while(ip)
	{
	if( !strcmp(ip->hostaddr,host) && !strcmp(ip->user,user) && 
	    !strcmp(ip->passwd,pw) )
		return TRUE;
	ip = ip->next;
	}
return FALSE;
}

int make_socket (unsigned short int port)
{
int 	sock;
struct 	sockaddr_in name;
int	optval = 1;
     
sock = socket (PF_INET, SOCK_STREAM, 0);
if (sock < 0) error_exit("socket");

setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval) );
name.sin_family = AF_INET;
name.sin_port = htons (port);
name.sin_addr.s_addr = htonl (INADDR_ANY);	
if (bind (sock, (struct sockaddr *) &name, sizeof (name)) < 0)
	error_exit("bind");
if (listen (sock, 1) < 0) error_exit("listen");
return sock;
}

int	get_free_channel()
{
int	chan;
for(chan  = 0; chan < CLIENT_LIMIT; ++chan)
	if(info->clients[chan].connected == FALSE) 
		return chan;
return -1;
}



/*
void test_for_life(int sock)
{
cmdpack cmd;

fprintf(stderr,"Sending a TEST_SOCKET packet...\n");
cmd.command = TEST_SOCKET;
if(send(sock,&cmd,sizeof(cmdpack),0) < 0)
	error_exit("test_socket");
}	
*/



int get_full_pack(int chan, void *buf, int size)
{
int nob,totnob = 0;
int	remaining = size;
charptr p = (charptr) buf;
while(remaining)		/* loop to get the full packet */
	{
	totnob += nob = recv(chan,p,remaining,0); 
	if(nob <= 0) 
		return nob;
	p += nob;
	remaining -= nob;
		}
return totnob;
}

void	new_connection(int cmdsock)
{
char	ipaddr[30];
cmdpack	cmd;
struct	sockaddr_in clname;
size_t	size;
int	tmp, ch;

size = sizeof(clname);
tmp = accept(cmdsock,(struct sockaddr*) &clname,&size);
if(tmp < 0) 
	error_exit("accept");
strcpy(ipaddr, inet_ntoa (clname.sin_addr));

/*
fprintf (stderr, "Command Channel: connect from host %s, port %hd.\n",
	ipaddr, ntohs (clname.sin_port));
*/

ch = get_free_channel();
if(! knownhost(ipaddr))		/* We don't want every machine here */
	{
	cmd.result = UNKNOWN_HOST;
	send(tmp, &cmd, sizeof(cmd), 0);
	close(tmp);
	}
else if(ch < 0)			/* Reached maximum number of clients */
	{
	cmd.result = TOO_MANY_CL;
	send(tmp, &cmd, sizeof(cmd), 0);
	close(tmp);
	}
else
	{
	cmd.result = DONE;
	if(send(tmp, &cmd, sizeof(cmd), 0) != sizeof(cmd) )
		return;
	info->clients[ch].connected = TRUE;
	info->clients[ch].cmdchan = tmp;
	strcpy(info->clients[ch].hostaddr, ipaddr);
	
/*	fprintf(stderr,"Channel %d opened.\n", info->clients[ch].cmdchan);
*/
	}
}


void verify_client(int ch)
{
cmdpack	cmd;
time_t	ct;

if(get_full_pack(info->clients[ch].cmdchan, &cmd, sizeof(cmdpack)) !=
		sizeof(cmdpack))
	{
	close_channel(ch);
	return;
	}

time(&ct);
if(! authorized_user(info->clients[ch].hostaddr, cmd.label,cmd.ascfunc))
	{
	printf("User Authorization Failed !!!!\nUser %s from host %s at %s",
		 cmd.label,info->clients[ch].hostaddr, ctime(&ct));
	cmd.result = WRONG_PASSWD;
	send(info->clients[ch].cmdchan, &cmd, sizeof(cmd), 0);
	info->clients[ch].connected = FALSE;
	close(info->clients[ch].cmdchan);
	info->clients[ch].cmdchan = 0;
	}
	else
	{
	cmd.result = DONE;
	send(info->clients[ch].cmdchan, &cmd, sizeof(cmd), 0);
	info->clients[ch].trusted = TRUE;
	strcpy(info->clients[ch].username, cmd.label);
	printf("Opened: User '%s' from host '%s' at %s", 
	info->clients[ch].username, info->clients[ch].hostaddr, ctime(&ct));
	}
}
