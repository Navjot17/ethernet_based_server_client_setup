#ifdef __cplusplus
extern "C" {
#endif

#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>

#include "common.h"

#define	CLIENT_LIMIT	5
#define	HOSTADDR_SIZE	16

#define TRUE		1
#define FALSE		0

#define DO_CAMIO	1000		/* driver ioctls */
#define	GET_JIFFI	1033

#define NMASK      0x3e00
#define AMASK      0x01E0
#define FMASK      0x001F
#define AVG_FACTOR 10

#define	DBASEID		"NSCPEL"

enum	datatype_vals {firstdata,ackclr,ackrd,ackwrt,metasn,metrange,metmant,
	chart,lin,logp,logn,nmlog,nmlogn,igmant,igchar,tcg,ignew,iph,ipl,
	fsbcd,fsdhi,fsdlo,pulse,doit,logical,bunch,mscmin,mscmax,mscfreq,lastdata};

enum    metertype_vals {firstmet,r0,r1,r2,rn1,autrng,lastmet};

enum	rangetype_vals {firstrange,aut,b0,b1,b2,b3,byp,cam,cbl,ccr,chg,cls,rngcol,
	crr,cntl,decr,dis,ena,flo,gnd,gvm,ins,inc,lcl,man,nlk,nml,
	nop,off,oil,opn,on,otm,otp,out,ovl,pad,prf,rdy,rem,rf,
	rtz,run,sht,slt,sum,tsf,wfm,wfp,rev,up,dwn,rnctl,rnpwr,standby,
	lib,hib,palarm,filam,pre,set,low,nylone,steel,lastrange};

enum	rectype_vals {firstrec,logdesc,numdesc,logcnaf,numcnaf,
	pulsecnaf,labelrec,lastrec};


				/* Server status information */
typedef	struct	{			
	int	connected, trusted;
	int	cmdchan;
	int	datachan;
	char	username[USERNAME_SIZE];
	char	hostaddr[HOSTADDR_SIZE];
	}client_info, *client_infoptr;

typedef	struct	{
	pid_t		newc_pid, datapid;
	boolean		all_channels_busy;
	client_info	clients[CLIENT_LIMIT];
	}serv_info, *serv_infoptr;

/*____________Structures used in the linked list database___________________*/
typedef	struct	cnafrec
	{
	int 	recid;			//enum 	rectype	recid;
	boolean	active;			/* Some CNAFs are marked off */
	byte	error_status;
	byte	crate;
	int16	naf;
	signed short intdat;
	struct	cnafrec	*nxtcnaf;
	} cnafrec,*cnafptr;

typedef	struct  numdes
	{
	int	func;			//enum	functype	func;
	int 	unit;			//enum	unitype		unit;
	int	data;			//enum  datatype	data;
	int	form;			//enum	formtype	form;
	int	met;			//enum	metertype	met;
	byte	datasize;
	real	upper_limit, lower_limit;
	real	m,c;
	boolean	averaging;
	byte	level;
	int32	avg_ary[AVG_FACTOR];
	cnafptr	cnap;
	struct	numdes	*nxtnum;
	} numrec,*numptr;

typedef	struct  logdes
	{
	int	func;			//enum	functype	func;
	int	unit;			//enum	unitype		unit;
	int	sw;			//enum	swtype		sw;	/* instead of datatype */
	int	hi,lo;			//enum	rangetype	hi,lo;
	byte	bitpos;
	cnafptr	cnap;
	struct	logdes	*nxtlog;
	} logrec,*logptr;

typedef	struct	labrec
	{
	char	labloc[LABLEN];
	numptr	firstnum;
	logptr	firstlog;
	struct	labrec	*nxtlab;
	} labrec,*labptr;

typedef	struct	headrec
	{
	char	userfileid[30];
	labptr	firstlab;
	cnafptr	firstcnaf;
	} headrec,*headptr;


/*------------------------------Globally known variables-------------------*/
extern	const   charptr	functypary[];
extern	const	charptr	unitypary[];
extern	const	charptr	datypary[];
extern	const	charptr	rangetypary[];
extern	const	charptr	metypary[];
extern	const	charptr	swtypary[];
extern	const	charptr	formtypary[];
extern	const	real 	twopower[];
extern	const	int32	dsizemask[];
extern	const	int32	bitmask[];
extern	serv_infoptr	info;

/*--------------------------- function prototypes-------------------------- */

int 	switch_on_toggle(charptr,logptr);	/* switch.c */
int 	switch_off_toggle(charptr,logptr);
int 	switch_on_momentory(charptr,logptr);
int 	switch_off_momentory(charptr,logptr);
int 	pulse_switch(charptr,logptr);
boolean	problem_cases(cmdpackptr);
boolean nlk_violation(logptr);


void	set_state(cmdpackptr);			/* servs.c */
void	get_state(cmdpackptr, boolean);
void	set_value(cmdpackptr);
void	get_value(cmdpackptr, boolean);
byte	calc_realval(charptr, numptr, realptr);
void	locate_remote_numerical(cmdpackptr cp);
void	locate_remote_logical(cmdpackptr cp);
void 	get_min(cmdpackptr cp);
void 	get_max(cmdpackptr cp);
void 	get_form(cmdpackptr cp);
void	get_low(cmdpackptr cp);
void	get_high(cmdpackptr cp);
void	get_swtype(cmdpackptr cp);


void	cp_camio(cnafptr);			/* camio.c */
boolean open_camac();
void 	periodic_camio();
				
void	error_exit(charptr);			/* utils.c */
boolean bitstatus(logptr);
void 	bitset(logptr);
void 	bitclr(logptr);

int 	make_socket (unsigned short int);	/* newcon.c */
int 	get_full_pack(int, void *, int);
void	new_connection(int);
void	verify_client(int);
void	receive_command(int);
boolean	load_iplist();

void	*shmalloc(size_t);			/* shmem.c */
void 	init_shared_memory();


headptr create_head();				/* dbchain.c */
cnafptr	locate_cnaf(byte, int16);
cnafptr create_cnaf(byte, int16);
void	attach_cnaf(cnafptr);
logptr	locate_logical(charptr, charptr, charptr);
void	attach_logical(labptr, logptr);
logptr	create_logical();
numptr	locate_numerical(charptr, charptr, charptr);
void	attach_numerical(labptr, numptr);
numptr	create_numerical();
labptr	locate_label(charptr);
labptr 	create_label(charptr);
void	attach_label(labptr);
labptr create_attach_label(charptr);

byte	is_in_list(char *p, const charptr *s);	/* globals.c */

boolean save_database(charptr);			/* load.c */
boolean load_database(charptr);
charptr calc_minmax(numptr, charptr);


void	close_channel(int ch);			/* pserv.c */
void	routine_jobs();
void	read_bpm(int ch, cmdpackptr cp);	/* bpm.c */
void	insert_bpm(cmdpackptr cp);
void	out_bpm(cmdpackptr cp);
void	locate_bpm(cmdpackptr cp);
int	setmodsw_on(cmdpackptr);
int	setmodsw_off(cmdpackptr);
int	set_cavitymux (int);
void	txfr_modbus (cnafptr, int, int, int);
byte	setdata_modio (int, signed short *, int);
byte	getdata_modio (int, signed short *, int);
boolean	init_bpm(void);

#ifdef __cplusplus
}
#endif

