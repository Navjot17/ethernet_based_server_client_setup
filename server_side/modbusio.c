#include "stmodio.h"
#include "pserv.h"

#define		SH_ERROR	8

extern uint8_t *tab_rq_status;
extern uint8_t *tab_rp_status;
extern uint16_t *tab_rq_registers;
extern uint16_t *tab_rp_registers;
extern int ret, nb_fail, nb_loop, addr, nb, selection_QWR;

extern int	data_in [8];
extern float 	data_in_f [8];
extern int	nslave;
extern int      regaddr;
extern int 	timer_test_ticks;

modbus_param_t	mbparam;

void init_modbus ()
{
	modbus_init_rtu (&mbparam, "/dev/ttyu0", 9600, "none", 8, 1);
	modbus_connect (&mbparam);
}

void txfr_modbus (cnafptr cp, int nslave, int modbusfunction, int subaddress)
{
	printf ("At txfr Slave: %d, Function: %d , Register address: %d.\n", nslave, modbusfunction, subaddress);
	switch (modbusfunction)
		{
	case READREG_MB:
		if (cp->active == FALSE)
			{
		//	printf("at cp->active false of READREG_MB");
			return;
			}
// local modbus transactions
		if (getdata_modio (nslave, &cp->intdat, subaddress))
		{
	//	printf ("At txfr modbus adc received is: %d.\n", cp->intdat);
			cp->error_status = SH_ERROR;
		}	else	{
			cp->error_status = 0;
		}
		break;

	case WRITEREG_MB :
		if (cp->active == FALSE)
		{//	printf("at cp->active false of READREG_MB");
// local modbus transactions
		printf ("Data 1 to DAC is %d %d  .\n", (short int) cp->intdat,nslave);
		if (setdata_modio (nslave, &cp->intdat, subaddress))
			cp->error_status = SH_ERROR;
		}
		else
			{
		printf ("Data to DAC is %d.\n", (short int) cp->intdat);
		if (setdata_modio (nslave, &cp->intdat, subaddress))
			cp->error_status = 0;
			}
		break;

	case READCOIL_MB:
		break;

	case WRITECOIL_MB:
		break;

	case GETSTAT_MB:
		break;

	default:
		break;
	}  

		return;
}

byte setdata_modio(int nslave, short int *data, int addr)
{
	int status;
	char buf [8];
	int i, dummy;
	short int *ip = (short int*) buf;
	*ip = *data;

	tab_rq_registers [0] = *ip; 
	printf ("Value: %d At setdata_modio status is %d Slave is: %d Adress is:%d.\n", (short int) *ip, status,nslave, addr);
	status = preset_single_register (&mbparam, nslave, addr, (unsigned int) (65535/*- 16383*/ + tab_rq_registers [0]));
	for (i=0; i<10000; ++i)
		dummy *= 1.011;
	if (ret != 1) {
		printf ("ERRRORRRR PRESET SINGLE REG (%d)\n", ret);
printf ("Slave = %d, address = %d, value = %d (0x%X)\n",nslave,addr,tab_rq_registers[0]);
		nb_fail++;
	}
	printf ("After comand%d %s status is %d.\n", *ip, ip, status);
	return 0;
}

byte getdata_modio(int nslave, signed short *data, int addr)
{
	int i, dummy, status;
	char buf [8];
	signed short *ip = (signed short *) buf;
//	*ip = *data;

//	tab_rq_registers [0] = *ip; 
//	 READ ADC

	nb = 8;
	ret = read_input_registers (&mbparam, nslave, 0, nb ,  tab_rq_registers); //read all 8 registers
	usleep (1000);
		if (ret != nb) {
//			printf ("Error read_input_registers\n");
	//		printf ("Slave = %d, address = %d, nb = %d\n", nslave, addr, nb);
			nb_fail++;
		}
		*data = (short int) tab_rq_registers [addr]+32768 ;
	printf ("data is  %d.\n", *data);
	return 0;
}

