/*****MSERV.C*****/
#include <signal.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pserv.h"
#include "serv_mod.h"


#define		MAXCAVITY	256	
extern struct modlabel *paramslist [];

extern time_t	last_update;

void	close_channel(int ch)
{
time_t	ct;
//time(&ct);
close(info->clients[ch].cmdchan);
printf("Closed: User '%s' from host '%s' at %s", 
	info->clients[ch].username, info->clients[ch].hostaddr, "Test" /* ctime(&ct)*/);
info->clients[ch].connected = FALSE;
info->clients[ch].trusted = FALSE;
info->clients[ch].cmdchan = 0;
}
 
void process_command(cmdpackptr cp)
{
int i, selection = 2;

//for (selection = 0; paramslist[selection]!=NULL && strcmp (cp->label, paramslist[selection]->labelname) && (selection < MAXCAVITY); ++selection)
			;
//	 printf ("Selection is %d.\n", selection);
	printf ("Received command % d.\n",cp->command);

switch(cp->command)
	{
	case LOCATE_NUMSIG:
		locate_remote_numerical(cp);
		printf ("Locating Num Sig. %s %d\n", cp->label, cp->result);
		break;

	case LOCATE_LOGSIG:
		locate_remote_logical(cp);
		printf ("Locating Log Sig.\n");
		break;

	case LOCATE_BPM:
		printf ("Locating BPM Sig.\n");
		locate_bpm(cp);
		break;

	case BPM_IN:
		printf ("Locating BPM IN Sig.\n");
		insert_bpm(cp);
		break;

	case BPM_OUT:
		printf ("Locating  BPM Out Sig.\n");
	 	out_bpm(cp);
		break;

	case GET_LOW:
		get_low(cp);
		printf ("Locating get low. %s %d\n",cp->label, cp->result);
		break;

	case GET_HIGH:
		get_high(cp);
		printf ("Locating Get Hi %s %d.\n",cp->label, cp->result);
		break;

	case GET_SWTYPE:
		get_swtype(cp);
		printf ("Locating Get SWT.\n");
		break;

	case GET_STATE:
		printf ("Locating GET STT.\n");
		get_state(cp, FALSE);
		break;

	case GET_STATE_NOW:
		printf ("Locating GET_STN.\n");
		get_state(cp, TRUE);
		break;

	case SET_STATE:
		printf ("Locating SET_STT====.\n");
		set_state(cp);
		break;

	case GET_VAL:
		get_value(cp, FALSE);
		printf ("Locating GET_VAL.\n");
		break;

	case GET_VAL_NOW:
		get_value(cp, TRUE);
		printf ("Locating GET_VAN.i\n");
		break;

	case GET_MIN:
		get_min(cp);
		printf ("Locating GET_MIN. %s %d\n",cp->label, cp->result);
		break;

	case GET_MAX:
		get_max(cp);
		printf ("Locating GET MAX. %s %d\n",cp->label, cp->result);
		break;

	case GET_FORM:
		get_form(cp);
		printf ("Locating GETFORM. %s %d\n",cp->label, cp->result);
		break;

	case SET_VAL:
		set_value(cp);
		printf ("Locating SET VAL. %s %d\n",cp->label, cp->result);
		break;

	default:
		cp->result = BAD_COMMAND;
	//	printf ("Locating BAD CMD. %s  %d\n",cp->label, cp->result);
		fprintf(stderr,"Unknown command\n");
	}

}

void	receive_command(int ch)
{
cmdpack	cmd;

if(get_full_pack(info->clients[ch].cmdchan, &cmd, sizeof(cmdpack)) !=
		sizeof(cmdpack) )
	{
	close_channel(ch);
	return;
	}

// printf ("Got full pack.\n");
if(cmd.command == CLOSE) {
	printf ("command is close.\n");
	close_channel(ch);
}

else
if(cmd.command == BPM_READ) {
	printf ("command is BPM.\n");
	read_bpm(ch, &cmd);
}
else
	{
//	printf ("processing cmd - %d.\n", cmd.command);
	process_command(&cmd);
	if(send(info->clients[ch].cmdchan, &cmd, sizeof(cmd), 0) < 0)
		close_channel(ch);
	}

/*				
if(cmd.result != DONE)
	printf("ERR: %s %s %s\n%s\n",
		cmd.label, cmd.ascfunc, cmd.ascunit,result_strings[cmd.result]);
*/

}

void	routine_jobs()
{
static	int ticks = 0;
//time(&last_update);
periodic_camio();
//if(!(ticks % 60))
	//{
		
//	save_database("restore");
/*	printf("Database written\n"); */
	//}
++ticks;
}
