/*****SWITCH.C*****/
#include <time.h>
#include <stdio.h>
#include <string.h>
#include "pserv.h"

static	char	momp_label[10];
static	logptr	active_momp = NULL;
boolean	pushing = FALSE;
time_t	ct;


/* 
	If Client Fails to Put off a Momentory Switch, Monitor does it.
	Only if it sees it ON twice without anybody pushing it.
 */

void momswitch_monitor()
{
if(active_momp && !pushing)
	{
	bitclr(active_momp);
	cp_camio(active_momp->cnap);

	time(&ct);
	printf("MSWM:%s %s %s going to %s state @ %s", momp_label,functypary[active_momp->func],
	rangetypary[active_momp->hi], rangetypary[active_momp->lo],ctime(&ct));

	active_momp = NULL;
	momp_label[0] = '\0';
	}
pushing = FALSE;

printf("MSMonitor.. first visit\n");
}

/* Depending on result returns the status code to be send to the client */


int switch_off_momentory(charptr label, logptr logp)
{

printf("In off Momsw\n");

if(!active_momp)
	{
	printf("No momentory pressed yet..\n");
	return DONE;
	}

if(logp != active_momp)
	{
	printf("Busy with some other momentory type signal\n");
	return JUST_FAILED;
	}
	
pushing = FALSE;
bitclr(logp);
cp_camio(logp->cnap);
if(logp->cnap->error_status)
	return CAMIO_ERROR;
time(&ct);
printf("M:%s %s %s going to %s state @ %s", label,functypary[logp->func],
	unitypary[logp->unit], rangetypary[logp->lo],ctime(&ct));
active_momp = NULL;
momp_label[0] = '\0';

return DONE;
}


int switch_on_momentory(charptr label, logptr logp)
{
if(active_momp)			/* something has been pushed earlier*/ 
	{
	if(active_momp == logp)
		{
		printf("Just Extending %s\n",label);
		pushing = TRUE;
		return DONE;
		}
	else
		{
		printf("Busy with some other momentory type signal\n");
		return 	JUST_FAILED; 
		}
	}		
		
bitset(logp);			/* A fresh assignment */
cp_camio(logp->cnap);
if(logp->cnap->error_status)
	return CAMIO_ERROR;
time(&ct);
printf("M:%s %s %s going to %s state @ %s", label,functypary[logp->func],
	unitypary[logp->unit], rangetypary[logp->hi],ctime(&ct));
active_momp = logp;
strcpy(momp_label,label); 
return DONE;
}

int pulse_switch(charptr label, logptr logp)
{
logp->cnap->active = TRUE;
cp_camio(logp->cnap);
logp->cnap->active = FALSE;
if(logp->cnap->error_status)
	return CAMIO_ERROR;
time(&ct);
printf("P:%s %s %s going to %s state@ %s", label,functypary[logp->func],
	unitypary[logp->unit], rangetypary[logp->hi],ctime(&ct));
return DONE;
}

int switch_on_toggle(charptr label, logptr logp)
{
if(nlk_violation(logp))
	return NLK_VIOLATION;
bitset(logp);
cp_camio(logp->cnap);
if(logp->cnap->error_status)
	return CAMIO_ERROR;
time(&ct);
printf("T:%s %s %s going to %s state @ %s", label,functypary[logp->func],
	unitypary[logp->unit], rangetypary[logp->hi],ctime(&ct));
return DONE;
}	

int switch_off_toggle(charptr label, logptr logp)
{
bitclr(logp);
cp_camio(logp->cnap);
if(logp->cnap->error_status)
	return CAMIO_ERROR;
time(&ct);
printf("T:%s %s %s going to %s state @ %s", label,functypary[logp->func],
	unitypary[logp->unit], rangetypary[logp->lo],ctime(&ct));
return DONE;
}

boolean	problem_cases(cmdpackptr cp)
{
logptr logp = locate_logical(cp->label, cp->ascfunc, cp->ascunit);
if(!logp)
	{
	cp->result = LOG_NOT_FOUND;
	return TRUE;
	}

if( !strcmp(cp->label,"PBPTK1") && !strcmp(cp->ascfunc,"SC") &&
	 !strcmp(cp->ascunit,"CTL") &&  !strcmp(cp->ascstate,"CHG") )
	{
	printf("M:%s %s %s: Going %s for 50 ms @ %s", cp->label,
	functypary[logp->func], unitypary[logp->unit], 
	rangetypary[logp->hi], ctime(&ct));
	
	bitset(logp);
	cp_camio(logp->cnap);
	if(logp->cnap->error_status)
		{
		cp->result = CAMIO_ERROR;
		return TRUE;
		}
	usleep(50000);		/* Push it for 50 msecs */
	bitclr(logp);
	cp_camio(logp->cnap);
	if(logp->cnap->error_status)
		{
		cp->result = CAMIO_ERROR;
		return TRUE;
		}
	cp->result = DONE;
	return TRUE;
	}

if( !strcmp(cp->label,"GS T 1") && !strcmp(cp->ascfunc,"SC") &&
	 !strcmp(cp->ascunit,"CTL") &&  !strcmp(cp->ascstate,"CHG") )
	{
	printf("M:%s %s %s: Going %s for 50 ms @ %s", cp->label,
	functypary[logp->func], unitypary[logp->unit], 
	rangetypary[logp->hi], ctime(&ct));

	bitset(logp);
	cp_camio(logp->cnap);
	if(logp->cnap->error_status)
		{
		cp->result = CAMIO_ERROR;
		return TRUE;
		}
	usleep(50000);
	bitclr(logp);
	cp_camio(logp->cnap);
	if(logp->cnap->error_status)
		{
		cp->result = CAMIO_ERROR;
		return TRUE;
		}
	cp->result = DONE;
	return TRUE;
	}
return FALSE;
}


boolean nlk_violation(logptr logp)
{
return FALSE;
}

