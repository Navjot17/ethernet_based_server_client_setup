#include "pserv.h"
#include "serv_mod.h"
#include <string.h>

char testchr [32][32];
struct modlabel *ptrmodparamalloc;
struct modlabel *paramslist [256];
FILE *file_params;
char buf_paramline [128];
int nparam, i, j;
 

load_paramlabels() {
	
	if (!(file_params = fopen ("modconf.ser", "rw")))
		{
		printf ("I could not load modbus details\n");
		return 0;
		}
	if (!fgets (buf_paramline, 127, file_params))
			return 0;
	for (j=0; !feof (file_params); ++j) {
		if (fgets (buf_paramline, 127, file_params)) {
		ptrmodparamalloc = (struct modlabel *) malloc (sizeof (struct modlabel));
		nparam = sscanf (buf_paramline, "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t", testchr[0], testchr[1], testchr[2], testchr[3], testchr[4], testchr[5], testchr[6], testchr[7], testchr[8], testchr[9], testchr[10], testchr[11], testchr[12], testchr[13], testchr[14], testchr[15]);
		strcpy (ptrmodparamalloc-> labelname, testchr [0]);
		ptrmodparamalloc-> type = testchr[1][0];	
		ptrmodparamalloc-> numrdbk_addr_MB = atoi(testchr [2]);
	       ptrmodparamalloc-> numrdbk_register_MB = atoi(testchr [3]);
		ptrmodparamalloc-> numrdbk_high_range = atoi(testchr [4]);
		ptrmodparamalloc-> numrdbk_low_range = atoi(testchr [5]);
		ptrmodparamalloc-> numrdbk_high_calibr = atoi(testchr [6]);
		ptrmodparamalloc-> numrdbk_low_calibr = atoi(testchr [7]);

		ptrmodparamalloc-> numctl_addr_MB = atoi(testchr [8]);
	       ptrmodparamalloc-> numctl_register_MB = atoi(testchr [9]);
		ptrmodparamalloc-> numctl_high_range = atoi(testchr [10]);
		ptrmodparamalloc-> numctl_low_range = atoi(testchr [11]);
		ptrmodparamalloc-> numctl_high_calibr = atoi(testchr [12]);
		ptrmodparamalloc-> numctl_low_calibr = atoi(testchr [13]);
		ptrmodparamalloc-> statctl1_addr_MB = atoi(testchr [14]);
		ptrmodparamalloc-> statctl1_coil_MB = atoi(testchr [15]);
		paramslist [j] = ptrmodparamalloc;
/*			printf ("The recorded label is %s\n",paramslist [j] -> labelname); 

			printf ("No of params %d\n", nparam);
			for (i=0;i<nparam; ++i)
			printf ("%s ", testchr[i]) ;
                        printf("\n");
			*/
		} else { 
		
		ptrmodparamalloc = 0;
		paramslist [j] = NULL;
		return 1;

	}
        	
  	}
