/*****CAMIO.C*****/
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include "pserv.h"

extern	headptr head;

#define	EMSG		1	/* error message printing */


#define DRIVER 		"/dev/hmcc"

#define	GENERR	   	1	/* As returned by the driver */ 
#define	NOXBIT	   	2
#define	NOQBIT	   	4
#define ERRMASK		7

#define SH_ERROR	8	/* Set by cp_camio routine */

const	int32	datamask = 0xffffff;
const	unsigned long crmask = (0xff << 16);
const	unsigned long fmask = 31;

int cfd;

static byte loc_camio(unsigned long cnaf,unsigned long *data)
{
int status;
int	fcode = cnaf & fmask;
char 	buf[8];
unsigned long *ip = (unsigned long *) buf;

*(ip+1) = cnaf;
if(fcode >= 16 && fcode <= 23) 	*ip = *data;

printf ("%d %s", *ip, ip);
/*
//			 returns zero, NOXBIT, NXQMASK or GENERR 
status = ioctl(cfd, DO_CAMIO, buf);

if( !status && (fcode <= 7) ) 	// if no error and read type, pick the data 
	*data = *ip;
	
return status & 7;
*/
return 0;
}	



		/* cp->error_status  will be set to ZERO or the error code */
void cp_camio(cnafptr cp)
{
int32	cnaf = (cp->crate << 16) | cp->naf;
					
if(cp->active == FALSE)
	return;

/* only local transactions. Ignores the crate number */

if(loc_camio(cnaf, &cp->intdat))
	cp->error_status = SH_ERROR;
else
	cp->error_status = 0;
return;
}



/*-------------------------------------------------------------------------*/
	/* fill the data into a circular buffer inside numerical record */	
void num_camio(numptr nump)
{				
cp_camio(nump->cnap);

if(nump->cnap->error_status)
	{
/*	printf("NUM CAMIO ERROR: \n"); */
	return;
	}
	
if( ! nump->averaging)
	return;

return;		/* don't do the avg business now */

nump->avg_ary[nump->level] = nump->cnap->intdat & dsizemask[nump->datasize];
++nump->level;
if(nump->level == AVG_FACTOR)
	nump->level = 0;
}

void periodic_camio()
{
labptr	labp = head->firstlab;
cnafptr	cnap = head->firstcnaf;

while(cnap)				/* do the logicals */
	{
	if(cnap->recid != numcnaf)
		cp_camio(cnap);
	cnap = cnap->nxtcnaf;
	}
	
while(labp)
	{
	numptr nump = labp->firstnum;
	while(nump)
		{
		num_camio(nump);

/*
printf("%s (%d) %6.2f\n",labp->labloc, nump->func, nump->m * nump->cnap->intdat + nump->c);
*/
		nump = nump->nxtnum;
		}
	labp = labp->nxtlab;
	}
}

boolean  open_camac()
{
cfd = open(DRIVER, O_RDWR);
if(cfd <= 0) 
	{ 
	printf("CAMAC driver open failed: %d\n", cfd); 
	return FALSE; 
	}
return TRUE;
}
