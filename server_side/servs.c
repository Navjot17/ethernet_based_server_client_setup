/*****SERVS.C*****/
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <modbus/modbus.h>
#include "pserv.h"
#include "stmodio.h"
#include "serv_mod.h"

#define		MAXCAVITY	30

extern struct modbusdev modbus_bus1 [];
extern char *Cavity_labels []; 
extern struct modlabel *paramslist[];
void setrelay (void);

extern modbus_param_t mbparam;

extern int ret; //selection_QWR;
extern int nslave, i;
extern int nb;
extern int addr;
extern int ret, nb_fail, nb_loop, addr, nb, selection_QWR;
extern uint8_t *tab_rq_status;
extern uint8_t *tab_rp_status;
extern uint16_t *tab_rq_registers;
extern uint16_t *tab_rp_registers;


/* These functions return static informations to the client */

void locate_remote_numerical(cmdpackptr cp)
{
if(locate_numerical(cp->label, cp->ascfunc, cp->ascunit))
	cp->result = DONE;
else
	cp->result = NUM_NOT_FOUND;
}

void locate_remote_logical(cmdpackptr cp)
{
if(locate_logical(cp->label, cp->ascfunc, cp->ascunit))
	cp->result = DONE;
else
	cp->result = LOG_NOT_FOUND;
}


void get_min(cmdpackptr cp)
{
char	ss[50];
numptr	nump = locate_numerical(cp->label, cp->ascfunc, cp->ascunit);

if(!nump)
	{
	cp->result = NUM_NOT_FOUND;
	return;
	}
calc_minmax(nump, ss);
//printf ("%s.\n", ss);
sscanf(ss,"%lf",&cp->value);	
cp->result = DONE;
}

void get_max(cmdpackptr cp)
{
double  min;
char	ss[50];
numptr	nump = locate_numerical(cp->label, cp->ascfunc, cp->ascunit);

if(!nump)
	{
	cp->result = NUM_NOT_FOUND;
	return;
	}
calc_minmax(nump, ss);
//printf ("%s.\n", ss);
sscanf(ss,"%lf %lf",&min, &cp->value);	
cp->result = DONE;
}

void	get_form(cmdpackptr cp)
{
numptr nump = locate_numerical(cp->label, cp->ascfunc, cp->ascunit);
if(!nump)
	{
	cp->result = NUM_NOT_FOUND;
	return;
	}
strcpy(cp->ascstate, formtypary[nump->form]);
//printf ("%s.\n", cp->ascstate);
cp->result = DONE;
}


void	get_low(cmdpackptr cp)
{
logptr	logp = locate_logical(cp->label, cp->ascfunc, cp->ascunit);
if(!logp)
	{
	cp->result = LOG_NOT_FOUND;
	return;
	}
strcpy(cp->ascstate, rangetypary[logp->lo]);
cp->result = DONE;
}

void	get_high(cmdpackptr cp)
{
logptr	logp = locate_logical(cp->label, cp->ascfunc, cp->ascunit);
if(!logp)
	{
	cp->result = LOG_NOT_FOUND;
	return;
	}
strcpy(cp->ascstate, rangetypary[logp->hi]);
cp->result = DONE;
}
	
void	get_swtype(cmdpackptr cp)
{
logptr	logp = locate_logical(cp->label, cp->ascfunc, cp->ascunit);
if(!logp)
	{
	cp->result = LOG_NOT_FOUND;
	return;
	}
strcpy(cp->ascstate, swtypary[logp->sw]);
cp->result = DONE;
}


/*-----------Following Sections return Dynamic Information---------------*/

void	set_state(cmdpackptr cp)	/* ON/OFF type controls */
{
int32	fcode;
int selection = 0;
int ret;
logptr	logp = locate_logical(cp->label, cp->ascfunc, cp->ascunit);
//	printf ("At setstate : %s Function: %s Unit: %s.\n", cp->label, cp->ascfunc, cp->ascunit);

for ( selection = 0; (paramslist [selection] != NULL) && strcmp (cp->label, paramslist [selection] -> labelname) && (selection  < 256); ++selection)
	;
//	printf ("%d %s %s.\n", selection, cp->label, Cavity_labels[selection]);
//	printf ("Selection is %d.\n", selection);
//	if (selection>=MAXCAVITY) 
//		return 0;

if(!logp)
	{
	cp->result = LOG_NOT_FOUND;
	return;
	}
fcode = logp->cnap->naf & FMASK;
if(fcode < 16 || fcode > 25)		/* Write group and F25 */
	{
	cp->result = LOG_READONLY;
return;
}

if( problem_cases(cp) )		/* Things like probe & gas strip */
return;


switch(logp->sw)
{
	case togsw:
		if(!strcmp(cp->ascstate, rangetypary[logp->hi])){
			cp->result = switch_on_toggle(cp->label,logp);	
//			printf ("Found the logical at on.\n");
			usleep (1000);
			ret = set_cavitymux(selection/3);
			setmodsw_on (cp);
		}
	
		else
		if(!strcmp(cp->ascstate, rangetypary[logp->lo])) {
			cp->result = switch_off_toggle(cp->label,logp);	
//			printf ("Found the logical at off.\n");
			usleep (1000);
			ret = set_cavitymux(selection/3);
			setmodsw_off (cp);
		}
		else
			cp->result = INVALID_STATE;
		break;

	case momsw:
		if(!strcmp(cp->ascstate, rangetypary[logp->hi]))
			cp->result = switch_on_momentory(cp->label,logp);	
		else
		if(!strcmp(cp->ascstate, rangetypary[logp->lo]))
			cp->result = switch_off_momentory(cp->label,logp);	
		break;

	case pulsw:
		if(!strcmp(cp->ascstate, rangetypary[logp->hi]))
			cp->result = pulse_switch(cp->label,logp);
		else
			cp->result = DONE;
		break;
	
	default:
		cp->result = SWTYPE_ERROR;
		break;
	}
}

void	get_state(cmdpackptr cp, boolean from_camac)
{
logptr	logp;

logp = locate_logical(cp->label, cp->ascfunc, cp->ascunit);
	//printf ("At getstate : %s Function: %s Unit: %s.\n", cp->label, cp->ascfunc, cp->ascunit);
if(!logp)
	{
	cp->result = LOG_NOT_FOUND;
	return;
	}

if(from_camac)
	cp_camio(logp->cnap);

if(bitstatus(logp))
	strcpy(cp->ascstate, rangetypary[logp->hi]);
else
	strcpy(cp->ascstate, rangetypary[logp->lo]);

if(logp->cnap->error_status)
	cp->result = CAMIO_ERROR;
else
	cp->result = DONE;

/*
fprintf(stderr, "In GS: %s %s %s %s (result = %d)\n",
cp->label, cp->ascfunc, cp->ascunit,cp->ascstate,cp->result);
*/

}

/*-----------------------------------------------------------------------*/

void	set_value(cmdpackptr cp)
{
int 	fcode, regaddr;
int 	selection = 0;
int 	ret;
double	newval = cp->value;
numptr	nump = locate_numerical(cp->label, cp->ascfunc, cp->ascunit);

//for ( selection = 0; strcmp (cp->label, Cavity_labels[selection]) && (selection  < MAXCAVITY); ++selection)
for ( selection = 0; (paramslist [selection] != NULL) && strcmp (cp->label, paramslist [selection] -> labelname) && (selection  < 256); ++selection)
	;

ret = set_cavitymux(selection/3);
 			if(paramslist[selection] == NULL){
						printf("Fatal ERROR in set\n");
						return;
						}
		nslave = paramslist [selection] -> numctl_addr_MB;
		regaddr = paramslist [selection] -> numctl_register_MB;

/*
switch (selection) {
	case 0: 
			regaddr =0;
			nslave =7;
			break;
	case 3: 
			regaddr =1;
			nslave =7;
			break;
	case 6: 
			regaddr =2;
			nslave =7;
			break;
	case 9:
			regaddr =3;
			nslave =7;
			break;
	case 12: 
			regaddr =0;
			nslave =8;
			break;
	case 15: 
			regaddr =1;
			nslave =8;
			break;
	case 18: 
			regaddr =2;
			nslave =8;
			break;
	case 21:
			regaddr =3;
			nslave =8;
			break;
	default:
			break;
	}	
*/
 printf ("Set value slave_num is %d.\n", nslave);

	printf ("At setvalue Label : %s Function: %s Unit: %s.\n", cp->label, cp->ascfunc, cp->ascunit);
if(!nump)
	{
	cp->result = NUM_NOT_FOUND;
	return;
	}
	
fcode = nump->cnap->naf & FMASK;
if(fcode < 16 || fcode > 23)
	{
	cp->result = NUM_READONLY;
	return;
	}
	
/*
if(newval > nump->upper_limit)
	{
	cp->result = ULIM_ERROR;
	return;
	}

if(newval < nump->lower_limit)
	{
	cp->result = LLIM_ERROR;
	return;
	}
*/
printf("the data is %d",nump->data);
switch(nump->data)
	{
	case lin:
	case mscmin:
	case mscmax:
		newval = (newval - nump->c) / nump->m;
		if((int)newval > dsizemask[nump->datasize] )
			{
			cp->result = DSIZE_ERROR;
			return;
			}
		nump->cnap->intdat = (int) (newval + 0.5);
//		nump->cnap->intdat *= -1;
//		cp_camio(nump->cnap);
		txfr_modbus(nump->cnap,nslave , WRITEREG_MB,regaddr);
	
		if(nump->cnap->error_status)
			{
			cp->result = CAMIO_ERROR;
			return;
			}
		cp->result = DONE;
		return;

	default:
		cp->result = UNKNOWN_DTYPE;
		return;
	}
}

/*-----------------------------------------------------------------------*/

int32 get_average_val(numptr nump)
{
int32	tmpi = 0;
short	i,k = 0;

/* if( !nump->averaging) */		/* no avg business now */
//	printf ("At get average val%d.\n", nump->cnap->intdat);
	return nump->cnap->intdat & dsizemask[nump->datasize];
	
for(i = 0; i < AVG_FACTOR; ++i)
	if(nump->avg_ary[i])
		{
		tmpi += nump->avg_ary[i];
		++k;
		}
if(!k)
	return 0;
return tmpi / k;
}

byte calc_realval(charptr label, numptr nump, realptr value)
{
int32 curdat = get_average_val(nump);

switch(nump->data)
	{
	case lin:
		*value = (double) curdat * nump->m + nump->c;
		return DONE;

	case ipl:
		if(curdat > 2045)
			nump = locate_numerical(label, "PRH", "T");
		if(!nump)
			return IPH_ERROR;
		curdat = get_average_val(nump);		/* fall to IPH */

	case iph:
		*value = (double) curdat * nump->m + nump->c;
		return DONE;

	case logp: case logn: case nmlog: case nmlogn:
		*value = (double) curdat * nump->m + nump->c;
		*value = pow(10.0, *value);
		return DONE;

	case tcg:
		if(curdat < 140)
			*value = 2000.0;
		else
			*value = 600.0/(nump->m * curdat) - nump->c;
		if(*value > 1000.0)
			*value = 1000.0;
		return DONE;

	case fsdlo:
		{
		int32 	loword, hiword, digit1, digit10, digit100;
		loword = get_average_val(nump);

		nump = locate_numerical(label, "DR", "FSH");
		if(!nump)
			return  FSDHI_ERROR;
		hiword = get_average_val(nump);

		loword = (loword >> 7) & 0x1f;
		hiword = (hiword >> 7) & 0x1f;
		digit1 = loword & 0xf;
		digit10 = (loword >> 4) + 2 * (hiword & 7);
		digit100 = hiword >> 3;

		*value = (real) (100 * digit100 + 10 * digit10 + digit1);
		return DONE;
		}

	default:
		return UNKNOWN_DTYPE;
	}
}


void get_value(cmdpackptr cp, boolean from_camac)
{
int regaddr;
int selection = 0;
int ret;
numptr	nump = locate_numerical(cp->label, cp->ascfunc, cp->ascunit);
	for ( selection = 0; (paramslist [selection] != NULL) && strcmp (cp->label, paramslist [selection] -> labelname) && (selection  < 256); ++selection)
	printf ("Test: %s \n", paramslist [selection] -> labelname);

//	if (selection>=MAXCAVITY) 
//		return 0;
//ret = set_cavitymux(selection/3);

if (paramslist [selection] == NULL) { 
					printf ("Fatal label error\n");
					return;
				}
if (strcmp (cp->ascfunc, "VC")) {
		nslave = paramslist [selection] -> numrdbk_addr_MB;
		regaddr = paramslist [selection] -> numrdbk_register_MB;
//		printf (" %d %d\n", paramslist [selection] -> numrdbk_addr_MB, paramslist [selection] -> numrdbk_register_MB);
		} else {
		nslave = paramslist[selection] -> numctl_addr_MB;
		regaddr = paramslist[selection] -> numctl_register_MB;
	//	printf ("%d %d\n", paramslist [selection] -> numctl_addr_MB, paramslist [selection] -> numctl_register_MB);
		}
/*

	{
switch (selection) {
	case 0: 
			regaddr =0;
			nslave =7;
			break;
	case 1: 
			regaddr =1;
			nslave =7;
			break;
	case 2: 
			regaddr =2;
			nslave =7;
			break;
	case 3: 
			regaddr =3;
			nslave =7;
			break;
	case 4:
			regaddr =0;
			nslave =8;
			break;
	case 5: 
			regaddr =1;
			nslave =8;
			break;
	case 6: 
			regaddr =2;
			nslave =8;
			break;
	case 7 : 
			regaddr =3;
			nslave =8;
			break;
	default:
			break;
	}	
	} else {	

switch (selection) {
	case 0 : 
			regaddr =0;
			nslave =2;
			break;
	case 1 : 
			regaddr =1;
			nslave =2;
			break;
	case 2 : 
			regaddr =2;
			nslave =2;
			break;
	case 3 : 
			regaddr =3;
			nslave =2;
			break;
	case 4 :
			regaddr =4;
			nslave =2;
			break;
	case 5 : 
			regaddr =5;
			nslave =2;
			break;
	case 6 : 
			regaddr =6;
			nslave =2;
			break;
	case 7 : 
			regaddr =7;
			nslave =2;
			break;
	default:
			break;
	}	
	}
*/
//printf ("Get value slave_num is %d.\n", nslave);


if(!nump)
	{
	cp->result = NUM_NOT_FOUND;
	return;
	}

		
if(from_camac)
	{
//	cp_camio(nump->cnap);
if (strcmp (cp->ascfunc, "VC"))
  { 
	txfr_modbus(nump->cnap, nslave, READREG_MB, regaddr);
//	printf ("Value at get_val %d.\n", nump->cnap->intdat);
	if(nump->cnap->error_status)
		{
		cp->result = CAMIO_ERROR;
		return;
			}
		}
	}

if(nump->cnap->error_status)
	cp->result = CAMIO_ERROR;
else
	{
//	printf ("Label: %s.\n", cp->label);
	cp->result = calc_realval(cp->label, nump, &cp->value);
	}
}

int set_cavitymux (int cavitynum)
{
float dummy=1.345;
addr = 0;
tab_rq_status = (uint8_t *) malloc (nb * sizeof (uint8_t));
memset (tab_rq_status, 0, nb * sizeof (uint8_t));

tab_rp_status = (uint8_t *) malloc (nb * sizeof (uint8_t));
memset (tab_rp_status, 0, nb * sizeof (uint8_t));

tab_rq_registers = (uint16_t *) malloc (nb * sizeof (uint16_t));
memset (tab_rq_registers, 0, nb * sizeof (uint16_t));

tab_rp_registers = (uint16_t *) malloc (nb * sizeof (uint16_t));
memset (tab_rp_registers, 0, nb * sizeof (uint16_t));

	nb=4;
	nslave =6;
	addr = 0;
//printf ("slave_num is %d.\n", nslave);
	tab_rq_status [0] = ((cavitynum) & 1) ? 1 : 0;
	tab_rq_status [1] = ((cavitynum) & 2) ? 1 : 0;
	tab_rq_status [2] = ((cavitynum) & 4) ? 1 : 0;
	tab_rq_status [3] = 0;
	ret = force_multiple_coils (&mbparam, nslave, addr, nb, tab_rq_status);
	usleep (1000);
	return ret;
}
