#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "pcli.h"


/*-------------------Server Transaction Section---------------------*/

double normalize(double mant, intptr exp, byteptr neg)
{		
			/* may be re-written using sprintf & sscanf */
*exp = 0;
*neg = FALSE;

if(mant == 0) return 0.0;

if(mant < 0) { *neg = TRUE; mant *= -1; }
	
if(mant >= 10.00000) while(mant >= 10.00000) { mant /= 10.0; ++*exp; }
else
if(mant < 1.00000) while(mant < 1.00000) { mant *= 10.0; --*exp; }
	
return mant;
}




void mark_for_update(int16 oldstatus, double oldval, numsigptr np)
{
int	exp, oldexp;
byte	oldsign;
double	mant, oldmant;
double	accuracy;

np->exp_update = np->mant_update = np->update = FALSE;  /* FALSE */

if (np->tran_status != oldstatus)
	np->update = TRUE;

if(np->tran_status != DONE)
	return;

oldmant = normalize(oldval, &oldexp, &oldsign);
mant = normalize(np->value, &exp, &np->mant_sign);


if(fabs(oldmant - mant) >= 0.0010)
	{
	np->meterman = mant;
	np->mant_update = TRUE;
	}

if(exp != oldexp)
	{	
	sprintf(np->ascmetexp,"%2d",exp);
	np->exp_update = TRUE;
	}



switch(np->form)
	{
	case f0: case f1: case f2: case f3:
	accuracy = 1.0 / pow(10,(double) (np->form - f0));
	
	if( fabs(oldval - np->value) >= accuracy/10)
		{
		np->update = TRUE;
		sprintf(np->ascval,"%10.*f", (int) (np->form-f0), np->value);
		}
	break;

	case e0: case e1: case e2:
	accuracy = 1.0 / pow(10,(double) (np->form - e0));

	if( (fabs(oldmant - mant) >= accuracy) || (oldexp != exp) )
		{
		np->update = TRUE;
		sprintf(np->ascval,"%6.*f %+02d", (int) (np->form-e0),mant,exp);
		}
	break;

	default:
	if( fabs(oldval - np->value) > 1.0)
		{
		np->update = TRUE;
		sprintf(np->ascval,"*%10.0f", np->value);
		}
	}
}


void	get_value_from_server(numsigptr np, boolean from_camac)
{
cmdpack cmd;
int16	oldstatus = np->tran_status;
double	oldvalue = np->value;

strcpy(cmd.label, np->name);
strcpy(cmd.ascfunc, np->func);
strcpy(cmd.ascunit, np->unit);
if(from_camac)
	cmd.command = GET_VAL_NOW;
else
	cmd.command = GET_VAL;

send_command (np->server_sock, &cmd);

np->tran_status = cmd.result;
if (cmd.result == DONE)		/* accept value if transaction is okey */
	np->value = cmd.value;	


//if(np->tran_status != DONE)
//	printf("Transaction  err.. %s\n",result_strings[cmd.result]);

	
mark_for_update(oldstatus, oldvalue, np);
}


void	get_value(numsigptr np)
{
get_value_from_server(np, FALSE);
}

void	get_value_now(numsigptr np)
{
get_value_from_server(np, TRUE);
}


boolean	set_value(numsigptr np, double value)
{
cmdpack cmd;
int16	oldstatus = np->tran_status;
double	oldvalue = np->value;


strcpy(cmd.label, np->name);
strcpy(cmd.ascfunc, np->func);
strcpy(cmd.ascunit, np->unit);
cmd.value = value;

cmd.command = SET_VAL;
send_command (np->server_sock, &cmd);

np->tran_status = cmd.result;
if (cmd.result != DONE)
	return FALSE;

np->value = cmd.value;	
mark_for_update(oldstatus, oldvalue, np);
return TRUE;
}

void	get_state_from_server(logsigptr lp, boolean from_camac)
{
cmdpack cmd;
int16	old_status = lp->tran_status;	

strcpy(cmd.label, lp->name);
strcpy(cmd.ascfunc, lp->func);
strcpy(cmd.ascunit, lp->unit);

if(from_camac)
	cmd.command = GET_STATE_NOW;
else
	cmd.command = GET_STATE;

send_command (lp->server_sock, &cmd);
lp->tran_status = cmd.result;

lp->update = FALSE;
if(old_status != lp->tran_status)
	lp->update = TRUE;
if ( (cmd.result == DONE) &&strcmp(lp->current_state, cmd.ascstate) )
	{
	strcpy(lp->current_state, cmd.ascstate);
		
	if(!strcmp(lp->current_state, "NOP") || 
	   !strcmp(lp->current_state, "NOP") )
		*lp->current_state = '\0';
	lp->update = TRUE;
	}

/*
fprintf(stderr, "GSN: %s %s %s %s ud = %d stat = %d\n",
lp->name, lp->func, lp->unit, lp->current_state, lp->update, lp->tran_status);
*/

}


void	get_state_now(logsigptr lp)
{
get_state_from_server(lp, TRUE);
}

void	get_state(logsigptr lp)
{
get_state_from_server(lp, FALSE);
}


boolean	set_state(logsigptr lp, charptr newstate)
{
cmdpack cmd;

strcpy(cmd.label, lp->name);
strcpy(cmd.ascfunc, lp->func);
strcpy(cmd.ascunit, lp->unit);

strcpy(cmd.ascstate, newstate);
cmd.command = SET_STATE;

send_command (lp->server_sock, &cmd);
lp->tran_status = cmd.result;

if (cmd.result != DONE)
	return FALSE;

strcpy(lp->current_state, newstate);
return TRUE;
}


void update_page_readbacks(int pn, boolean force_update)	
{
onepageptr	p;
logsigptr 	lp;
numsigptr 	np;
int		dn, fld;


if( (pn < 0) || (pn >= MaxPages) )
	return;
	
p = &page_array[pn];

for(dn = 0; dn < DevPerPage; ++dn)
	{
	if( !p->dev[dn].active)
		continue;
	
	lp = p->dev[dn].srd;
	for(fld = 0; fld < SRDMAX; ++fld, ++lp)
	   if(lp->active)
	   	{
		get_state(lp);   
		if(force_update)
			lp->update = TRUE;
		}

	np = p->dev[dn].nrd;
	for(fld = 0; fld < NRDMAX; ++fld, ++np)
	   if(np->active)
	   	{
		get_value(np);
/*
	printf("Update: %s %s %s %s (%d)\n",
		np->name, np->func, np->unit, np->ascval, np->update);
*/				
		if(force_update)
			np->update = TRUE;
		}
	}
}





/*------------------Server Connection Establishment Section-----------*/

char	ipfile[] = "server.list";

server_details	serv_info[MAX_SERV];
int		num_servers = 0;


int get_connection(charptr server, int port)
{
	int	s;
	struct sockaddr_in sin;
	struct hostent *hp;
	charptr	p;

	s = socket (AF_INET, SOCK_STREAM, 0);
	if (s < 0) {
		perror ("socket (client)");
		exit (0);
	}
	
	/* go find out about the desired host machine */
	hp = gethostbyname (server);
	if (hp == NULL) {
		fprintf (stdout, "Unknown host %s.\n", server);
		return -1;
	}
	
	/* fill in the socket structure with host information */
	sin.sin_family = AF_INET;
	sin.sin_port = htons (port);
	memcpy (&sin.sin_addr, hp->h_addr, hp->h_length);
	p = inet_ntoa (sin.sin_addr);

	if (connect (s, (struct sockaddr *) &sin, sizeof (sin)) < 0) 
		{
		fprintf(stdout,"Host %s is down or Server is not running.\n",
			server);
		return -1;
		}
     					
	p = inet_ntoa (sin.sin_addr);
	strcpy(serv_info[num_servers].host, p);
	
	return s;
}

int get_full_pack (int chan, void *buf, int size)
{
	int 	nob,totnob = 0;
	int 	remaining = size;
	charptr p = (charptr) buf;

	/* loop to get the full packet */
	while(remaining) {
		totnob += nob = recv (chan, p, remaining, 0); 
		if(nob < 0) {
			perror ("Client : get_full_pack");
			exit (0);
		}
		p += nob;
		remaining -= nob;
	}
	return totnob;
}



void send_command (int chan, cmdpackptr cp)
{
	int nc;

	if ((nc=send (chan,cp,sizeof(cmdpack), 0)) < 0)
		error_exit("send");
	get_full_pack (chan, cp, sizeof(cmdpack));
}

int open_server (charptr server, charptr user, charptr passwd)
{
	cmdpack cmd;
	int	cmdchan = get_connection(server,CMD_PORT);

	if(cmdchan < 0)
		return cmdchan;
		
	get_full_pack (cmdchan, &cmd, sizeof(cmdpack));
         
	if (cmd.result != DONE)
                {
		return -1;
                }
        
	strcpy (cmd.label, user);
	strcpy (cmd.ascfunc, passwd);
	send_command (cmdchan, &cmd);
	if (cmd.result != DONE)
		{
		fprintf(stdout,"Host %s Says you gave %s\n",
			serv_info[num_servers].host,result_strings[cmd.result]);
		return -1;
		}

	return cmdchan;
}


void open_all_servers (charptr user, charptr passwd)
{
	int	cmdchan;
	char	hostaddr[50];
	FILE	*fp = fopen(ipfile,"rt");       //ipfile = server.list

	if (!fp)
		error_exit ("No Server List");

	while (!feof(fp)) 
		{
	        fscanf (fp, "%s", hostaddr);
	        if (hostaddr[0] == '$')
       		 	break;
		printf("%s\n", hostaddr);		
		cmdchan = open_server (hostaddr, user, passwd);
		if (cmdchan < 0)
                        {
		        printf("Searching to host %s.(sock = %d)\n",
			          serv_info[num_servers].host, cmdchan);		
			continue;
                        }
		serv_info[num_servers].chan = cmdchan;
		printf("Connected to host %s.(sock = %d)\n",
			serv_info[num_servers].host, cmdchan);		

		++num_servers;
		}
if(!num_servers)
	{
	fprintf(stdout,"Can't Attach to any server.......exiting!!!!\n");
	exit(0);
	}
}

