#ifndef	__NSC__PCLI__
#define	__NSC__PCLI__

#include <Xm/Xm.h>

#include "common.h"

#define TRUE		1
#define FALSE		0

#define	PELCON_HEADING		"Pelletron Control Project"

#define	ASCVLEN		15
#define	EXPVLEN		4
#define	SRDMAX		3
#define SCTLMAX		3
#define NRDMAX		2
#define	NCTLMAX		2
#define	DevPerPage	16
#define	MaxPages	100
#define	MaxBPM		2
#define	MaxACK		4
#define	PageTitleSize	100
#define	SmallTitleSize	25

enum	sigtype {logical, numerical};
enum 	states	{low, high, max_states};

typedef
	struct logsignal {
		enum	sigtype stype;
		boolean	active,update;
		short	tran_status;
		int	server_sock;
		char	name[LABLEN];
		char	func[FUNCLEN];
		char	unit[UNITLEN];
		char	states[max_states][STATELEN];
		char	current_state[STATELEN];
		boolean	mom_switch;	/* treated in different manner */
	} logsig, *logsigptr;

typedef
	struct numsignal {
		enum	sigtype stype;
		boolean	active, update, mant_update, exp_update;
		short	tran_status;
		int	server_sock;
		char	name[LABLEN];
		char	func[FUNCLEN];
		char	unit[UNITLEN];
		char	ascval[ASCVLEN];
		char	ascmetexp[EXPVLEN];
		enum	formtype form;
		double	value,min,max;
		double	meterman;
		byte	mant_sign;
		double	knob_saved_value;
	} numsig, *numsigptr;
	

typedef
	struct shdev {
		boolean	active, update;
		char	name[10];
		logsig	srd[SRDMAX], sctl[SCTLMAX];
		numsig	nrd[NRDMAX], nctl[NCTLMAX];
	} shdev, *shdevptr;
	

typedef
	struct onepage {
		boolean	active;
		char	title [PageTitleSize];
		char	small_title [SmallTitleSize];
		shdev	dev [DevPerPage];
	} onepage, *onepageptr;

typedef	onepage	pagestore [MaxPages];


#define	MAX_SERV	5

typedef	struct
	{
	char	host[20];
	int	chan;
	}server_details;
	

/*---------------------- X-window related stuff--------------------- */

#define	ProgName	"Pcon"

		/* The following dimensions goes to Resource File "Pcli" */

#define	PageFormWidth		600
#define	BpmFormWidth		320
#define ComdFormWidth		70
#define	MeterFormWidth		PageFormWidth
#define	KnobFormWidth		(PageFormWidth + BpmFormWidth + ComdFormWidth)
#define	ScaleFormWidth		KnobFormWidth

#define	PageFormHeight		420		
#define	MeterFormHeight		230		
#define	BpmFormHeight		580	


#define	OFFSET			4
#define	SHADOW			2
#define	Fix_L		XmNleftAttachment, XmATTACH_FORM
#define	Fix_T		XmNtopAttachment, XmATTACH_FORM
#define	Fix_R		XmNrightAttachment, XmATTACH_FORM
#define	Fix_B		XmNbottomAttachment, XmATTACH_FORM
#define	Fix_LT		Fix_L, Fix_T	
#define	Fix_LTRB	Fix_L, Fix_T, Fix_R, Fix_B	
#define	OFix_L		Fix_L, XmNleftOffset, OFFSET
#define	OFix_T		Fix_T, XmNtopOffset, OFFSET
#define	OFix_R		Fix_R, XmNrightOffset, OFFSET
#define	OFix_B		Fix_B, XmNbottomOffset, OFFSET
#define	OFix_LT		OFix_L, OFix_T
#define	OFix_LTRB	OFix_L, OFix_T, OFix_R, OFix_B

#define Offset4		XmNleftOffset,4,XmNtopOffset,4,\
XmNrightOffset,4,XmNbottomOffset,4

enum	programStates { idle_state, selected_meter, selected_knob, 
	selected_scale, inHelpMode };

typedef	struct 		/* Shared information for X-related stuff */
	{
	Widget		top, mainForm, titleForm, arrowForm, inputForm, dateForm,
			pageForm, meterForm, knobForm,
			bpmForm, scaleForm,
			msgForm, commandForm;
	
	XtAppContext	apc;
	
	Colormap	cm;
	Display		*dpy;

	Pixel		DevNameError, DevNameBack, 
			MeterBack, MeterNeedle, MeterUnit, MeterTicks,
			BpmBack,BpmGrid, BpmDraw;
	
	enum programStates	current_state;
	unsigned int		assign_number;

	}x_information;
			

/*------------------------Globally known variables---------------*/

extern	const	charptr	functypary[];
extern	const	charptr	unitypary[];
extern	const	charptr	datypary[];
extern	const	charptr	rangetypary[];
extern	const	charptr	metypary[];
extern	const	charptr	swtypary[];
extern	const	charptr	formtypary[];

extern	pagestore	page_array;
extern	int		loaded_pages[];
extern	int		number_of_pages_loaded;
extern	int		CurPage;

extern	int		MasterTimerInterval;
extern	x_information	x_info;

/*------------------------function prototypes--------------------*/

void	error_exit(charptr);				/* pcli.c */

boolean	valid_entry (char *p, const charptr *s);	/* globals .c */
byte	is_in_list(charptr p, const charptr *s);

boolean load_page (int);				/* pgload.c */
boolean	located_logical_details (logsigptr lp);
boolean	located_analog_details (numsigptr lp);

int 	make_socket (unsigned short int);		/* socks.c */
int 	get_full_pack (int, void *, int);
void	new_connections (void);
int 	get_connection (charptr, int);
void 	open_all_servers (charptr user, charptr passwd);
void 	send_command (int, cmdpackptr);
boolean	set_state(logsigptr lp, charptr newstate);
void	get_state(logsigptr lp);
void	get_state_now(logsigptr lp);
boolean	set_value(numsigptr lp, double);
void	get_value(numsigptr np);
void	get_value_now(numsigptr np);
void	update_page_readbacks (int, boolean);




void	init_display(int argc, charptr *argv);		/* xwin.c */
void set_label_string(Widget w, charptr p);

void create_page_widgets(void);				/* page.c */
void change_displaypage (int pgnum);
void refresh_status_reads (int devnum);
void update_page (void);

void doit_service (Widget, XtPointer, XtPointer);	/* doit.c */
void momswitch_service (void);


void 	create_meter_widgets (void);			/* meter.c */
void	init_meters(void);
void	assign_meter(int, int);
void	remove_meter(int);
void	update_meters(void);

void 	create_scale_widgets (void);			/* scale.c */
void	init_scales(void);
void	assign_scale(int, int);
void	remove_scale(int);
void	update_scales(void);

void 	create_knob_widgets (void);			/* knob.c */
void	init_knobs(void);
void	assign_knob(int, int);
void	remove_knob(int);
void	update_knobs(void);
boolean  init_knob_hardware(void);


void 	create_bpm_widgets (void);			/* bpm.c */
void	init_bpms(void);
void	update_bpms(void);

void 	create_command_widgets (void);			/* command.c */
void 	create_input_widgets (void);	
void	display_message(charptr);
void	load_log_input(void);
void	load_mag_names(void);


void 	ddisp_service(void);				/* ddisp.c */
boolean	load_ddisp_input(void);

#endif
