#include <stdio.h>
#include <stdlib.h>

#include "pcli.h"


void error_exit (char *error)
{
	perror (error);
	exit (0);
}


extern	boolean knob_hardware_present;

int main(int argc, char **argv)
{
	int	pgnum;


	knob_hardware_present =  init_knob_hardware();

//	if(!knob_hardware_present) exit(0);

	open_all_servers ("ajith","kumar");

	fprintf(stderr, "Processing Pages..\n");
	for (pgnum=0; pgnum<MaxPages; ++pgnum)
		{
		if(load_page (pgnum))
			fprintf(stderr,"%d ",pgnum);
		}

	fprintf(stderr, "\nLoading Log Input File.\n");
	load_log_input();
	fprintf(stderr, "Loading Diagramatic Display & Interlocks.\n");
	load_ddisp_input();
	load_mag_names();

	init_display (argc, argv);
	return 0;
}
