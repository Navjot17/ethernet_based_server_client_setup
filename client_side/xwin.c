#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <Xm/Form.h>
#include <Xm/Label.h>

#include "pcli.h"




boolean	knob_hardware_present = 0;

extern	charptr prog_name;

int	 MasterTimerInterval = 100;		/* 100 msec */



void create_forms(void)
{
x_info.mainForm = XtVaCreateManagedWidget("mainForm",
	xmFormWidgetClass, x_info.top,
	Fix_LTRB,
	NULL);

x_info.titleForm  = XtVaCreateManagedWidget("titleForm",
	xmFormWidgetClass, x_info.mainForm,
	Fix_LT,
	NULL);

x_info.arrowForm  = XtVaCreateManagedWidget("arrowForm",
	xmFormWidgetClass, x_info.mainForm,
	Fix_T,
	XmNleftWidget, x_info.titleForm,
	XmNleftAttachment, XmATTACH_WIDGET,
	XmNbottomWidget, x_info.titleForm,
	XmNbottomAttachment, XmATTACH_OPPOSITE_WIDGET,
	NULL);

x_info.pageForm  = XtVaCreateManagedWidget("pageForm",
	xmFormWidgetClass, x_info.mainForm,
	Fix_L,
	XmNtopWidget, x_info.titleForm,
	XmNtopAttachment, XmATTACH_WIDGET,
	NULL);

x_info.dateForm  = XtVaCreateManagedWidget("dateForm",
	xmFormWidgetClass, x_info.mainForm,
	Fix_T,Fix_R,
	XmNbottomWidget, x_info.titleForm,
	XmNbottomAttachment, XmATTACH_OPPOSITE_WIDGET,
	NULL);
	
x_info.inputForm  = XtVaCreateManagedWidget("inputForm",
	xmFormWidgetClass, x_info.mainForm,
	Fix_T,
	XmNleftWidget, x_info.arrowForm,
	XmNleftAttachment, XmATTACH_WIDGET,
	XmNrightWidget, x_info.dateForm,
	XmNrightAttachment, XmATTACH_WIDGET,
	XmNbottomWidget, x_info.titleForm,
	XmNbottomAttachment, XmATTACH_OPPOSITE_WIDGET,
	NULL);

x_info.meterForm  = XtVaCreateManagedWidget("meterForm",
	xmFormWidgetClass, x_info.mainForm,
	Fix_L,
	XmNtopWidget, x_info.pageForm,
	XmNtopAttachment, XmATTACH_WIDGET,
	NULL);


x_info.bpmForm  = XtVaCreateManagedWidget("bpmForm",
	xmFormWidgetClass, x_info.mainForm,
	XmNleftWidget, x_info.pageForm,
	XmNleftAttachment, XmATTACH_WIDGET,
	XmNtopWidget, x_info.pageForm,
	XmNtopAttachment, XmATTACH_OPPOSITE_WIDGET,
	NULL);

x_info.msgForm  = XtVaCreateManagedWidget("msgForm",
	xmFormWidgetClass, x_info.mainForm,
	XmNleftWidget, x_info.bpmForm,
	XmNleftAttachment, XmATTACH_OPPOSITE_WIDGET,
	XmNtopWidget, x_info.bpmForm,
	XmNtopAttachment, XmATTACH_WIDGET,
	XmNbottomWidget, x_info.meterForm,
	XmNbottomAttachment, XmATTACH_OPPOSITE_WIDGET,
	XmNrightWidget, x_info.bpmForm,
	XmNrightAttachment, XmATTACH_OPPOSITE_WIDGET,
	NULL);


x_info.commandForm  = XtVaCreateManagedWidget("commandForm",
	xmFormWidgetClass, x_info.mainForm,
	XmNleftWidget, x_info.bpmForm,
	XmNleftAttachment, XmATTACH_WIDGET,
	XmNbottomWidget, x_info.meterForm,
	XmNbottomAttachment, XmATTACH_OPPOSITE_WIDGET,
	XmNtopWidget, x_info.pageForm,
	XmNtopAttachment, XmATTACH_OPPOSITE_WIDGET,
	NULL);


if(knob_hardware_present)
	x_info.knobForm  = XtVaCreateManagedWidget("knobForm",
	xmFormWidgetClass, x_info.mainForm,
	Fix_L,
	XmNtopWidget, x_info.meterForm,
	XmNtopAttachment, XmATTACH_WIDGET,
	NULL);
else
	x_info.scaleForm  = XtVaCreateManagedWidget("scaleForm",
	xmFormWidgetClass, x_info.mainForm,
	Fix_L,
	XmNtopWidget, x_info.meterForm,
	XmNtopAttachment, XmATTACH_WIDGET,
	NULL);
}





/******************Get All necessary Color Pixel Values*****************/

char	*DevNameError = "red";
char	*DevNameBack = "steel blue";
char	*MeterBack  = "white";
char	*MeterNeedle  = "white";
char	*MeterTicks  = "black";
char	*MeterUnit = "steel blue";

char	*BpmBack = "dim grey"; 
char	*BpmGrid = "green4";
char	*BpmDraw = "green";

boolean init_colors(void)
{
XColor	screen, exact;

x_info.dpy = XtDisplay(x_info.top);
x_info.cm = DefaultColormap(x_info.dpy, 0);

if(XAllocNamedColor(x_info.dpy, x_info.cm, DevNameError, &screen, &exact) == 0)
	return FALSE;
x_info.DevNameError = screen.pixel; 

if(XAllocNamedColor(x_info.dpy, x_info.cm, DevNameBack, &screen, &exact) == 0)
	return FALSE;
x_info.DevNameBack = screen.pixel; 

if(XAllocNamedColor(x_info.dpy, x_info.cm, MeterBack, &screen, &exact) == 0)
	return FALSE;
x_info.MeterBack = screen.pixel; 

if(XAllocNamedColor(x_info.dpy, x_info.cm, MeterTicks, &screen, &exact) == 0)
	return FALSE;
x_info.MeterTicks = screen.pixel; 

if(XAllocNamedColor(x_info.dpy, x_info.cm, MeterNeedle, &screen, &exact) == 0)
	return FALSE;
x_info.MeterNeedle = screen.pixel; 


if(XAllocNamedColor(x_info.dpy, x_info.cm, MeterUnit, &screen, &exact) == 0)
	return FALSE;
x_info.MeterUnit = screen.pixel; 


if(XAllocNamedColor(x_info.dpy, x_info.cm, BpmBack, &screen, &exact) == 0)
	return FALSE;
x_info.BpmBack = screen.pixel; 

if(XAllocNamedColor(x_info.dpy, x_info.cm, BpmGrid, &screen, &exact) == 0)
	return FALSE;
x_info.BpmGrid = screen.pixel; 


if(XAllocNamedColor(x_info.dpy, x_info.cm, BpmDraw, &screen, &exact) == 0)
	return FALSE;
x_info.BpmDraw = screen.pixel; 


return TRUE;
}


/**************************Date & Time Display******************************/

struct
	{
	Widget	form, date, hour, sec; 
	}date;


void create_date_widgets(void)
{
date.form = XtVaCreateManagedWidget("date",
   	xmFormWidgetClass, x_info.dateForm,
	OFix_LTRB,
	XmNshadowThickness, SHADOW,
	XmNshadowType, XmSHADOW_IN,
	NULL);

date.sec = XtVaCreateManagedWidget("sec",
   	xmLabelWidgetClass, date.form,
	OFix_T, OFix_B, OFix_R,
	NULL);


date.hour = XtVaCreateManagedWidget("hour",
   	xmLabelWidgetClass, date.form,
	XmNrightWidget, date.sec,
	XmNrightOffset, OFFSET,
	XmNrightAttachment, XmATTACH_WIDGET,
	OFix_T, OFix_B,
	NULL);

date.date = XtVaCreateManagedWidget("date",
   	xmLabelWidgetClass,  date.form,
	OFix_L, OFix_T, OFix_B,
	XmNrightWidget, date.hour,
	XmNrightOffset, OFFSET,
	XmNrightAttachment, XmATTACH_WIDGET,
	NULL);
}


void update_date ()
{
static	struct	tm old_tm, *tm;
time_t	t;
char	ss[128];

time (&t);
tm = localtime (&t);

if ((tm->tm_mday != old_tm.tm_mday) || (tm->tm_mon != old_tm.tm_mon) || 
	(tm->tm_year != old_tm.tm_year)) 
	{
	sprintf (ss, "%2d/%2d/%2d",tm->tm_mday, tm->tm_mon+1, tm->tm_year);
	set_label_string (date.date, ss);
	}

if (tm->tm_min != old_tm.tm_min)
	 {
	sprintf (ss, "%2d:%02d", tm->tm_hour, tm->tm_min);
	set_label_string (date.hour, ss);
	}

sprintf (ss, ":%02d ", tm->tm_sec);
set_label_string (date.sec, ss);
	
memcpy (&old_tm, tm, sizeof (struct tm));
}

/************************************************************************/




void set_label_string(Widget w, charptr p)
{
XmString	s;

s = XmStringCreateLocalized (p);
XtVaSetValues (w, XmNlabelString, s, NULL);
XmStringFree (s);
}





void  master_timer (Widget w, XtPointer client, XtPointer call)
{
static	int	ticks = 0;

momswitch_service ();

update_meters ();

update_scales ();

update_knobs ();

if (! (ticks % 4)) update_bpms ();

if (! (ticks % 5)) { update_page (); update_date(); ddisp_service(); }

++ticks;

XtAppAddTimeOut (x_info.apc, MasterTimerInterval,
		(XtTimerCallbackProc) master_timer, (XtPointer) NULL);
}




void init_display(int argc, char **argv)
{

x_info.top = XtVaAppInitialize( &x_info.apc, prog_name,
            NULL, 0,            /* command line option list */
            &argc, argv,        /* command line args */
            NULL,               /* for missing app-defaults file */
            NULL);              /* terminate varargs list */


/* knob_hardware_present =  init_knob_hardware(); */

create_forms();

create_date_widgets();
create_page_widgets();
create_meter_widgets();
create_bpm_widgets();
create_command_widgets();

if(knob_hardware_present)
	create_knob_widgets();
else
	create_scale_widgets();

XtRealizeWidget(x_info.top);

if( !init_colors())
	{
	fprintf(stderr, "Failed to get colormap\n");
	exit(0);
	}

init_meters();
init_bpms();

if(knob_hardware_present)
	init_knobs();
else
	init_scales();

change_displaypage(loaded_pages[0]);


XtAppAddTimeOut (x_info.apc, MasterTimerInterval,
		(XtTimerCallbackProc) master_timer, (XtPointer) NULL);


XtAppMainLoop(x_info.apc);
}
