#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <math.h>

#include <X11/cursorfont.h>
#include <Xm/Xm.h>
#include <Xm/Form.h>
#include <Xm/Label.h>
#include <Xm/PushB.h>

#include "pcli.h"


#define READ_KNOB	1000
#define WRITE_KNOB	1001
#define VERIFY_KNOB	1002
#define READ_SRBYTE	1003


#define		KNOB_WIDTH	(KnobFormWidth/4 - OFFSET)
#define		KNOB_HEIGHT	40

#define MaxKnobs	4

typedef struct
	{
	Widget		form, name, value, unit;
	boolean		asnflag;
	numsigptr	devnp;
	real		m,c;
	int		last_read_value;
	boolean		value_changed;
	} knobrec, *knobptr;

knobrec		knob_info[MaxKnobs];
Widget		knobRC;


#define KDAT	0
#define	KNUM	1
unsigned long	buf[KNUM+1];		/* a two element array */
int		kfd;
unsigned int	knob_resol = 0xffff;

char		ss[100];

int	read_sr_byte()
{
if(ioctl(kfd, READ_SRBYTE, &buf))
	return FALSE;
return (buf[KDAT] & 0xff);
}



boolean	read_knob(int kn, intptr data)
{
int	check, delta;

buf[KNUM] = kn;
ioctl(kfd, READ_KNOB, &buf);
*data = buf[KDAT];

buf[KNUM] = kn;			/* Verify for the PC I/O problem */
ioctl(kfd, READ_KNOB, &buf);
check = buf[KDAT];
delta = abs(*data - check);
if(delta > 5)
	{
	printf("Read Verify: delta = %d\n", delta);
	return FALSE;
	}

// Slow down KNOB 0 16 times - ETS
if (kn == 0)
	{
	int	change = *data - knob_info [kn].last_read_value;
// slow down by 16	
	change /= 16;	// Divide by 16
// Do not change the value when it is assigned for the first time
	if (! knob_info [kn].last_read_value)  return TRUE;
	*data = knob_info [kn].last_read_value + change;

// Write it back to SHAFT ENCODER so that the change is recorded
	buf[KNUM] = kn;
	buf[KDAT] = *data;
	ioctl(kfd, WRITE_KNOB, &buf);
// Addition by ETS ends
	}

return TRUE;
}



boolean	write_knob(int kn, int data)
{
int	tmpi;

buf[KNUM] = kn;
buf[KDAT] = data;
ioctl(kfd, WRITE_KNOB, &buf);

read_knob(kn, &tmpi);			/* Verify for the PC I/O problem */
if( abs(data - tmpi) > 4)
	ioctl(kfd, WRITE_KNOB, &buf);
return TRUE;
}



boolean  init_knob_hardware()
{
//int kn;

kfd = open("/dev/knob", O_RDWR);
if(kfd <= 0) 
	{ 
	printf("KNOB driver open failed: %d\n", kfd); 
	return FALSE; 
	}
/*
for(kn = 0; kn < MaxKnobs; ++kn)
	{
	buf[KNUM] = kn;
	if( ioctl(kfd, VERIFY_KNOB, &buf) || (buf[KDAT] == 0))
		{
		printf("Init failed for knob %d\n",kn); 
		return FALSE;
		}
	}
*/

printf("Hardware Detected for %d Knobs\n", MaxKnobs);
return TRUE;
}


void 	assign_knob(int dn, int fld)
{
unsigned int	i, kn = x_info.assign_number;
knobptr		kp;
numsigptr	newsig = &page_array [CurPage].dev [dn].nctl [fld]; 
double		tmpf;

if( (kn >= MaxKnobs) || ! newsig->active)
	return;

for(i = 0; i < MaxKnobs; ++i)
	if( knob_info[i].asnflag && 
	! strcmp(newsig->name, knob_info[i].devnp->name) &&
	! strcmp(newsig->unit, knob_info[i].devnp->unit) )
		{
		sprintf(ss,"Knob %d is with %s", i, knob_info[i].devnp->name);
		display_message(ss);
		return;
		}

kp = &knob_info[kn];
kp->devnp = &page_array[CurPage].dev[dn].nctl[fld];

get_value_now(kp->devnp);

kp->m =  (kp->devnp->max - kp->devnp->min) / knob_resol;
kp->c = kp->devnp->min;
tmpf = (kp->devnp->value - kp->c) / kp->m;

kp->last_read_value = 0;
write_knob(kn, (int) (tmpf + 0.5));
kp->last_read_value = (int) (tmpf + 0.5);

kp->asnflag = TRUE;
set_label_string(kp->name, kp->devnp->name);
set_label_string(kp->value, kp->devnp->ascval);
set_label_string(kp->unit, kp->devnp->unit);

sprintf(ss,"Assigned Knob %d to %s",kn+1,kp->devnp->name);
display_message(ss);
}


void update_ascval(numsigptr np)
{
switch(np->form)
	{
	case f0: case f1: case f2: case f3:
		sprintf(np->ascval,"%10.*f", (int) (np->form-f0), np->value);
		break;

	default:
		sprintf(np->ascval,"*%10.0f", np->value);
		break;
	}
}



void update_knobs(void)
{
int	kn;
int	sr_status;


sr_status = read_sr_byte();
if(read_sr_byte() != sr_status) sr_status = 0;

for(kn = 0; kn < MaxKnobs; ++kn)
   if(knob_info[kn].asnflag)
	{
	knobptr	kp = &knob_info[kn];
	int	tmpi;
	double	tmpf;
	
	if(sr_status & (2 << (2*kn)) )		/* if restore requested */
		{
		tmpf = (kp->devnp->knob_saved_value - kp->c) / kp->m;
		tmpi = (int) (tmpf + 0.5);
		write_knob(kn, tmpi);
		kp->value_changed = TRUE;
		printf("\a");
		}
	else					/* or read the SE */
		{				
		if(!read_knob(kn, &tmpi))
			continue;
		}

/*
//added for error trapping
	if(abs(tmpi-kp->last_read_value) > 0)
		{
		char	ss[100];
		sprintf(ss,"Knob %d: from %d to %d\n", 
			kn, kp->last_read_value, tmpi);
		display_message(ss);
		if(abs(tmpi-kp->last_read_value) > 1000)
			{
			write_knob(kn, kp->last_read_value);
			return;
			}
		}
*/				
	if(tmpi != kp->last_read_value)
		{
		set_value(kp->devnp, kp->m * tmpi + kp->c);
		kp->devnp->update = TRUE;
		kp->value_changed = TRUE;
		kp->last_read_value = tmpi;
		}
	else			
		{
		double tmpf;
		
		get_value_now(kp->devnp);
		if(kp->devnp->update)		/* someone else changed it */
			{
			tmpf = (kp->devnp->value - kp->c) / kp->m;
			tmpi = (int) (tmpf + 0.5);
			write_knob(kn, tmpi);
			kp->value_changed = TRUE;
			}
		}
		
	if(sr_status & (1 << (2*kn) ) )
		{
		kp->devnp->knob_saved_value = kp->devnp->value;
		printf("\a");
		}

	if(kp->devnp->update || kp->value_changed)
		{
		update_ascval(kp->devnp);
		set_label_string(kp->value, kp->devnp->ascval);
		}
	}
}


void	remove_knob (int kn)
{
if(kn >= MaxKnobs) return;
	
set_label_string(knob_info [kn].name, " ");
set_label_string(knob_info [kn].value, " ");
set_label_string(knob_info [kn].unit, " ");
knob_info[kn].asnflag = False;

sprintf(ss,"Cancelled Knob %d",kn+1);
display_message(ss);
}


void mark_knob_for_assign(Widget w, XtPointer client_data, XtPointer call_data)
{
if(x_info.current_state != idle_state)
	{
	x_info.current_state = idle_state;
	display_message("Cancelled Selection");
	return;
	}

x_info.current_state = selected_knob;
x_info.assign_number = (unsigned int) client_data;

sprintf(ss,"Selecteded Knob %d", x_info.assign_number+1);
display_message(ss);
}


void init_knobs ()
{
int 	kn;
for (kn = 0; kn < MaxKnobs; ++kn) 
	{
	set_label_string(knob_info [kn].name," ");
	set_label_string(knob_info [kn].value," ");
	set_label_string(knob_info [kn].unit," ");
	knob_info[kn].asnflag = False;
	}
}


void 	create_knob_widgets (void)
{
int	kn;

for(kn = 0; kn < MaxKnobs; ++kn)
	{
	knobptr kp = &knob_info[kn];
	int 	x = kn * (KNOB_WIDTH + OFFSET) + OFFSET;
	
	kp->form = XtVaCreateManagedWidget("kform",
	        xmFormWidgetClass, x_info.knobForm,
	        XmNx, x,
	        OFix_T, OFix_B, 
	        XmNwidth, KNOB_WIDTH,
	        XmNheight, KNOB_HEIGHT,
	        XmNshadowThickness, SHADOW,
        	NULL);
        	
	kp->name = XtVaCreateManagedWidget("kname",
	        xmPushButtonWidgetClass,kp->form,
        	OFix_L, OFix_T,OFix_B,
        	NULL);
        	
	kp->value = XtVaCreateManagedWidget("kvalue",
	        xmLabelWidgetClass, kp->form,
        	OFix_T, OFix_B,
		XmNleftOffset, OFFSET,
        	XmNleftWidget, kp->name,
        	XmNleftAttachment, XmATTACH_WIDGET,
        	NULL);

	kp->unit = XtVaCreateManagedWidget("kunit",
	        xmLabelWidgetClass, kp->form,
        	OFix_T, OFix_B, OFix_R,
		XmNleftOffset, OFFSET,
        	XmNleftWidget, kp->value,
        	XmNleftAttachment, XmATTACH_WIDGET,
        	NULL);

        	
	XtAddCallback (kp->name, XmNactivateCallback, 
		mark_knob_for_assign, (XtPointer) kn);
	}
}
