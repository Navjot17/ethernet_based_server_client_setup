#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "pcli.h"


extern	server_details	serv_info[];
extern	int		num_servers;
extern	pagestore	page_array;
extern	int		loaded_pages_array[];
extern	int		number_of_pages_loaded;

char	current_label[10] = "$$$";
int	current_index;
int	current_pgnum;
boolean	valid_label;


void snap_shot(void);


boolean	located_logical_details (logsigptr lp)
{
	cmdpack cmd;
	int	sock;

	lp->server_sock = -1;

	strcpy (cmd.label, lp->name);		/* hunt all servers */
	strcpy (cmd.ascfunc, lp->func);
	strcpy (cmd.ascunit, lp->unit);
	cmd.command = LOCATE_LOGSIG;
					
	for (sock=0; sock<num_servers; ++sock) {
		send_command (serv_info[sock].chan, &cmd);
		if (cmd.result == DONE) {
			lp->server_sock = serv_info [sock].chan;
			break;
		}
	}

	if (lp->server_sock == -1)
		return FALSE;

	cmd.command = GET_LOW;
	send_command (lp->server_sock, &cmd);
	if (cmd.result != DONE)
		return FALSE;
	strcpy (lp->states[low], cmd.ascstate);

	cmd.command = GET_HIGH;
	send_command (lp->server_sock, &cmd);
	if (cmd.result != DONE)
		return FALSE;
	strcpy (lp->states[high], cmd.ascstate);

	cmd.command = GET_SWTYPE;
	send_command (lp->server_sock, &cmd);
	if (cmd.result != DONE)
		return FALSE;
	if (!strcmp (swtypary[momsw], cmd.ascstate))
		lp->mom_switch = TRUE;
	else
		lp->mom_switch = FALSE;

	strcpy(lp->current_state, "???");	/* force a first update */
	get_state_now(lp);			/*  get_state(lp) is faster */


	return TRUE;
}



boolean	located_analog_details (numsigptr np)
{
	cmdpack cmd;
	int	sock;


	np->server_sock = -1;

	strcpy(cmd.label, np->name);
	strcpy(cmd.ascfunc, np->func);
	strcpy(cmd.ascunit, np->unit);
	cmd.command = LOCATE_NUMSIG;

	for(sock=0; sock<num_servers; ++sock) {
		send_command (serv_info[sock].chan, &cmd);
		if (cmd.result == DONE) {
			np->server_sock = serv_info[sock].chan;
			break;
		}
	}
	if (np->server_sock == -1)
		return FALSE;
		
	cmd.command = GET_FORM;
	send_command (np->server_sock, &cmd);
	if (cmd.result != DONE)
		return FALSE;
	np->form = is_in_list(cmd.ascstate, formtypary);	

	cmd.command = GET_MIN;
	send_command (np->server_sock, &cmd);
	if (cmd.result != DONE)
		return FALSE;
	np->min = cmd.value;	

	cmd.command = GET_MAX;
	send_command (np->server_sock, &cmd);
	if (cmd.result != DONE)
		return FALSE;
	np->max = cmd.value;	

	np->value = -1e6;		/* to force an update */
	get_value_now(np);		/* get_value(np) is faster */

	return TRUE;
}



#define FL	20
boolean	parse_analog(charptr func, charptr unit, charptr ss)
{
	char	dummy[FL], ascfunc[FL], ascunit[FL];
	boolean	valid;

	if( 3 != sscanf(ss,"%s %s %s", dummy, ascfunc, ascunit))
		return FALSE;

	valid = valid_entry(ascfunc,functypary) && 
	    valid_entry(ascunit,unitypary); 
	if(!valid)
		return FALSE;

	strcpy(func, ascfunc);
	strcpy(unit, ascunit);
	return TRUE;
}



boolean	parse_logical(charptr func, charptr unit, charptr ss)
{
	char	dummy[FL], ascfunc[FL], ascunit[FL];
	boolean	valid;

	if( 3 != sscanf(ss,"%s %s %s", dummy, ascfunc, ascunit))
		return FALSE;

	valid = valid_entry(ascfunc,functypary) && 
	    valid_entry(ascunit,unitypary);
	if(!valid)
		return FALSE;

	strcpy(func, ascfunc);
	strcpy(unit, ascunit);
	return TRUE;
}


boolean	add_label(charptr ss)
{
	char	dummy[FL], asclab[FL];
	int	index;

	if( 3 != sscanf(ss,"%s %s %d", dummy, asclab, &index))
		return FALSE;

	if ((strlen (asclab) >= LABLEN) || (index < 0) || (index >= DevPerPage))
		return FALSE;

	strcpy (current_label, asclab);
	current_index = index;
	valid_label = TRUE;

	strcpy(page_array [current_pgnum].dev [index].name, current_label);
	page_array [current_pgnum].dev [index].active = TRUE;
	return TRUE;
}



boolean	add_status_read(int fld, charptr ss)
{
	logsigptr lp = &page_array[current_pgnum].dev [current_index].srd[fld];

	if(!valid_label)
		return FALSE;
		
	if(!parse_logical(lp->func, lp->unit, ss))
		return FALSE;
		
	strcpy(lp->name, current_label);

	if( !located_logical_details(lp))
		return FALSE;

	lp->active = TRUE;
	return TRUE;
}



boolean	add_status_control(int fld, charptr ss)
{
	logsigptr lp = &page_array[current_pgnum].dev [current_index].sctl[fld];

	if(!valid_label)
		return FALSE;

	if(!parse_logical(lp->func, lp->unit, ss))
		return FALSE;

	strcpy(lp->name, current_label);

	if( !located_logical_details(lp))
		return FALSE;

	lp->active = TRUE;
	return TRUE;
}



boolean	add_analog_read(int fld, charptr ss)
{
	numsigptr np = &page_array[current_pgnum].dev [current_index].nrd[fld];

	if(!valid_label)
		return FALSE;

	if(!parse_analog(np->func, np->unit, ss))
		return FALSE;

	strcpy(np->name, current_label);

	if( !located_analog_details(np))
		return FALSE;

	np->active = TRUE;
	return TRUE;
		
}



boolean	add_analog_control(int fld, charptr ss)
{
	numsigptr np = &page_array[current_pgnum].dev [current_index].nctl[fld];

	if(!valid_label)
		return FALSE;

	if(!parse_analog(np->func, np->unit, ss))
		return FALSE;

	strcpy(np->name, current_label);

	if( !located_analog_details(np))
		return FALSE;

	np->active = TRUE;
	return TRUE;
}


boolean add_small_title(charptr ss)
{
char	dummy[FL], st[256];
 
if( 2 != sscanf(ss,"%s %s", dummy, st))
	return FALSE;

if (strlen (st) < SmallTitleSize -1 )
	{
	strncpy(page_array [current_pgnum].small_title, st, SmallTitleSize-1);
	return TRUE;
	}

return FALSE;
}



boolean load_page(int pgnum)
{
	char	*str, ss[256];
	FILE	*fp;
	boolean	located;

	if ( (pgnum < 0) || (pgnum >= MaxPages) )
		return FALSE;

	sprintf (ss,"page/page%02d.pge",pgnum);
	fp = fopen(ss,"rt");
	if (! fp) 
		return FALSE;

	memset (& page_array [pgnum], 0, sizeof (onepage));
	current_pgnum = pgnum;

	fgets (ss, 256, fp);
	strncpy(page_array [current_pgnum].title, ss, 80);
	if ((str=strchr (page_array [current_pgnum].title, '\n')) != NULL)
		*str = 0;

	while (!feof(fp))
	{
		fgets (ss, 256, fp);
		
		if (isspace (ss [0]))
			continue;
		located = FALSE;
		
		if (! strncmp (ss, "SMALL:", 6))
				located = add_small_title (ss);
		else 
		if (! strncmp (ss, "LABEL:", 6))
				located = add_label (ss);
		else 
		if (! strncmp (ss, "SR1:", 4) &&
			!page_array[pgnum].dev[current_index].srd[0].active)
				 located = add_status_read (0, ss);
		else 
		if (! strncmp (ss, "SR2:", 4) &&
			!page_array[pgnum].dev[current_index].srd[1].active)
				located = add_status_read (1, ss);
		else 
		if (! strncmp (ss, "SR3:", 4) &&
			!page_array[pgnum].dev[current_index].srd[2].active)
				located = add_status_read (2, ss);
		else 
		if (! strncmp (ss, "SC1:", 4) &&
			!page_array[pgnum].dev[current_index].sctl[0].active)
				located = add_status_control (0, ss);
		else 
		if (! strncmp (ss, "SC2:", 4) &&
			!page_array[pgnum].dev[current_index].sctl[1].active)
				located = add_status_control (1, ss);
		else 
		if (! strncmp (ss, "SC3:", 4) &&
			!page_array[pgnum].dev[current_index].sctl[2].active)
				located = add_status_control (2, ss);
		else 
		if (! strncmp (ss, "AR1:", 4) &&
			!page_array[pgnum].dev[current_index].nrd[0].active)
				located = add_analog_read (0, ss);
		else 
		if (! strncmp (ss, "AR2:", 4) &&
			!page_array[pgnum].dev[current_index].nrd[1].active)
				located = add_analog_read (1, ss);
		else 
		if (! strncmp (ss, "AC1:", 4) &&
			!page_array[pgnum].dev[current_index].nctl[0].active) 
				located = add_analog_control (0, ss);
		else 
		if (! strncmp (ss, "AC2:", 4) &&
			!page_array[pgnum].dev[current_index].nctl[1].active)
				located = add_analog_control (1, ss);
		else 
		if(! strncmp (ss, "END:", 4))
			{
			located = TRUE;
			valid_label = FALSE;
			}
			
		if( ! located)
			fprintf (stdout, "Page %d: Device %s: Could not Resolve Signal %s\n",
				current_pgnum, current_label, ss);
	}
	fclose (fp);
	if(!strlen(page_array [current_pgnum].small_title))
		sprintf(page_array[current_pgnum].small_title,"Page %d",pgnum);

	page_array [pgnum].active = TRUE;

	if(number_of_pages_loaded < MaxPages)
		loaded_pages[number_of_pages_loaded++] = current_pgnum;
	
	return TRUE;
}




/*
void snap_shot(void)
{
int dn, fld,pgnum = current_pgnum;

printf("Located Signal and Field information of Page %d\n",pgnum);
for(dn = 0; dn < 16; ++dn)
	{
	numsigptr np;
	logsigptr lp;
	
	lp  = &page_array[pgnum].dev[dn].srd[0];
	for(fld = 0; fld < SRDMAX; ++fld, ++np)
	    if(lp->active)
		printf("DEV(%2d) SRD(%d): %s %3s %3s a = %d s = %d\n",
			dn, fld, lp->name,lp->func, lp->unit, 
			lp->active, lp->server_sock);

	lp = &page_array[pgnum].dev[dn].sctl[0];
	for(fld = 0; fld < SCTLMAX; ++fld, ++np)
	    if(lp->active)	
		printf("DEV(%2d) SCT(%d): %s %3s %3s a = %d s = %d\n", 
			dn, fld,lp->name,lp->func, lp->unit, 
			lp->active, lp->server_sock);


	np  = &page_array[pgnum].dev[dn].nrd[0];
	for(fld = 0; fld < NRDMAX; ++fld, ++np)
	    if(np->active)	
		printf("DEV(%2d) NRD(%d): %s %3s %3s a = %d s = %d\n",
			dn, fld, np->name,np->func, np->unit,
			np->active, np->server_sock);

	np = &page_array[pgnum].dev[dn].nctl[0];
	for(fld = 0; fld < NCTLMAX; ++fld, ++np)
	    if(np->active)
		printf("DEV(%2d) NCT(%d): %s %3s %3s a = %d s = %d\n",
			dn, fld, np->name,np->func, np->unit,
			np->active, np->server_sock);

	}
printf("\n");
}

*/
